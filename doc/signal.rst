Signal Processing
=================

.. toctree::
   :maxdepth: 2


Signal processing occur at each run.

Peak detection
---------------

This procedure is repeated for each detection channel (photodiode).

The droplets are detected by looking for continuous regions over a
detection threshold using a smoothed (convolution by a Blackman window)
and transformed (``x-median/mean_absolute_deviation_to_the_median``)
signal. This define the ``peak_start`` and ``peak_end`` of each droplet.
A peak region (``region_start``, ``region_end``) is also defined by
taking the midpoint between successive stop and start.

Implementation
^^^^^^^^^^^^^^
.. autofunction:: dropsignal.processing.detect_peaks
		  :noindex:
Peak Measure
------------

This procedure is repeated for each measurement channel
(photomultipliers).

| For all peak, we define a masked region between ``peak_start`` and
  ``peak_end`` in the associated detection channel.
| If ``alignment_correction`` is true, this mask is shifted
  (individually for each peak) within the region so the (cumulative)
  signal within the mask is maximum. This allow to correct for
  unavoidable hardware missalignment. Typically, you should set it to
  ``true`` for fluorescence channels and ``false`` for light scattering.

The boundaries of the masked region define the ``peak_start`` and
``peak_end`` for this channel.

Here are examples:

|image0|

|image1|


Implementation
^^^^^^^^^^^^^^
.. autofunction:: dropsignal.processing.measure_peaks
		  :noindex:


.. |image0| image:: img/detec01.png
.. |image1| image:: img/detec02.png
