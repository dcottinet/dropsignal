.. Dropsignal documentation master file, created by
   sphinx-quickstart on Thu Feb  1 15:47:07 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Dropsignal's documentation!
======================================

Dropsignal is a a free and open source (AGPL3+) software software
performing the extraction, conversion and visualization of
experimental data obtained from millifluidic droplet trains.

.. toctree::
   :maxdepth: 2

   manual
   overview
   signal
   changelog
   API <api/modules>

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
