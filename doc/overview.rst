Overview of the pipeline
========================

.. toctree::
   :maxdepth: 1

Here is a quick description of what the pipeline actually does:

-  **Peak detection**:

   For each run and each detection channel, the droplets are detected by
   looking for continuous regions over a ``detection threshold`` using a
   smoothed (convolution by a Blackman window) and transformed
   (x-median/mean\_absolute\_deviation\_to\_the\_median) signal.

   Droplet size and speed is computed by comparing several detection
   channels

-  **Peak Measure**

   For all peak, a masked region with the same width as the region above
   the detection threshold in the detection channel is created.

   For all channels in ``correct_alignment_with_detection``, this mask
   is shifted (individually for each peak) of ``shift`` measure points
   so the convolution product between the measurment channel and the
   detection channel is maximum.

   Then the following are computed:

   id\_run
       Index of the peak in the run (starting at 0)
   max
       Maximum on the ``peak_inter[i], peak_inter[i+1]`` region
   mean
       Mean signal on the masked region
   median
       Median signal on the masked region
   std
       Standard deviation on the masked region
   cov
       Coefficient of variation (Standard deviation over mean) on the
       masked region

-  **Run Quality check**: A short quality check is done on each run to
   tag the ones that are too short, too long or have too many/few
   droplets (compared to the expected number given in the protocol). If
   a run has a different number of droplet than the previous and the
   next, but those are equal, it is tagged as "probable\_misscount". If
   a droplet was lost, the run is tagged as "coalescence\_event" and the
   following as "coalesced". If more than one droplet were lost the run
   is tagged as "dramatic\_coalescence".
-  **Droplet dynamics reconstruction**: Rebuild individual dynamics by
   aggregating the runs and correcting the indices to take into account
   coalescence. "id\_run" refers to the position of the measured droplet
   in the current run, whereas "id\_exp" refers to the index of the
   dynamics this measure point has been assigned to.
-  **Droplet annotation**
-  **Replicate averaging**
-  **Plugins application**

Please take a look at the `README
<https://gitlab.com/evomachine/dropsignal/blob/master/README.md>`__
file for a more technical description of the library stack (if you
want to run it yourself for instance).
