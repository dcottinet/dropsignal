=========
Changelog
=========

.. toctree::
   :maxdepth: 1

New in 0.4 (Unreleased)
=======================

Follow the progress of this version `on the issue tracker. <https://gitlab.com/evomachine/dropsignal/milestones/3>`_

**New features:**

- Added "Export sorting list" function to the Scatter page.
- Added the ``dropsignal_web`` command and the multiple experiment
  dashboard. This allow to start and open analyses from the browser.
- Added a web-based protocol editor accessed from the
  ``dropsignal_web`` dashboard.
- Added proper documentation: https://evomachine.gitlab.io/dropsignal/
- Added the ``absolute_time`` and ``relative_time`` columns in outputs
  ``droplets/{id}.csv`` and ``droplet_dynamics``. The first one is
  identical to the old ``time`` column and the second is translated by
  the droplet creation time (i.e. the ``absolute time`` measured in
  the sipper head at run 0 for this droplet).
- Added the ``relative_times`` option in ``protocol.json``, if set to
  ``true``, the ``time`` column will contain relatives times rather
  than absolute times. This column is used by the plugins and the
  visualization interface.
- Added some eye candy in the web interface: loader animation and
  favicon.
- Added experimental light scattering support

**Modifications:**

- Changed the cache folder which is now ``cache/`` instead of the
  root of the experiment to avoid clutter. You can safely remove all
  the ``raw*.pkle`` files in your experiment folders.
- Changed the way the first run (``run 0``) is processed: measurement
  are now only done on the sipper head (``pmt4``), 10 droplets are
  trimmed on both sides and the time of each peak is recorded. All the
  other channels are ignored (and not displayed). This allow to find the droplet creation
  time.

**Fixes:**

- Fixed memory issues with the server and reduced the amount of data
  pre-loaded in memory when using the serve command.
- Fixed bug preventing to display the raw signal on machines that only
  have one optical block.
- Fixed overlapping of wells when the plate is not 96 wells in dynamics tab.
- Fixed overflow when there is a lot of groups in dynamics tab.

**Deprecated:**

- Deprecated ``dropsignal_gui`` interface that will be removed in 1.0,
- Deprecated ``dropsignal single_run`` command that will be removed in 1.0,
- Deprecated ``--plot`` flag that will be removed in 1.0.
- Deprecated ``dropsignal index`` flag that will be removed in 1.0.


New in 0.3 (24th October 2017)
==============================

New Raw data explorer
---------------------

The ``Signal`` tab have been completely reworked and allow the
interactive exploration of the complete raw signal.

Use the buttons at the top to select a run, channel. You can use the
subplot at the bottom to explore the run or jump directly to a given
droplet or coalescence event using the buttons at the top of the tab.

You can toggle the display of the individual measure using the checkbox
in the top right.

Hovering the peaks display a tooltip giving you information about the
current measure. Clicking on the peak will center and lock the view on
the droplet.

When looking at a fluorescence channel, the shaded area on each peak is
the actual window on which the average signal is computed in order to
get the ``mean`` value of this droplet.

-  The width of this area is determined by the number of points that are
   above the ``detection_threshold`` in the corresponding ``detection``
   channel.
-  This window may **not** be centered on the peak in the ``detection``
   channel (However the peak index text is always directly above the
   peak in the ``detection`` channel). Instead, it is shifted for each
   droplet and channel to maximize the convolution product. The extent
   of the shift is displayed in the tooltip (only for fluorescence
   channels).

To use the new ``Signal`` tab, dropsignal server must be running. It is
started when using the GUI ``Start`` or ``Serve`` buttons or the new
command ``dropsignal serve .``. Otherwise you will be able to fallback
on the old ``Signal`` tab.

**dropsignal now depend on the cherrypy package**, if you have troubles
with the new version check that cherrypy is installed (e.g via
``pip install cherrypy``). A fresh install of ``dropsignal`` should
install ``cherrypy``.

Peak detection caching
----------------------

The pipeline cache the peak detection in files called
``Raw[xxx]_[hash].pkle``. As a consequence, if the pipeline is run again
with the same peak detection parameters, the run are not processed
again, which reduce greatly the running time.

Improved Scatter tab
--------------------

The ``Scatter`` tab now keeps the memory of the selected variable in the
URL. This allows to bookmark a given configuration of the scatter plot
and also allows to keep the configuration in case of page refresh.

The scatter plot have an improved tooltip, similar to the one in the new
``Signal`` tab.

Clicking on a dot in the scatter plot now send you on the corresponding
peak in the raw data explorer.

New color options
~~~~~~~~~~~~~~~~~

You can use the ``protocol.json`` entry ``color_palette`` to give the
pipeline a list of colors that will be used to distinguish the groups
across all visualizations.

Moreover, you can specify a given color for certain groups using the
``color_group`` entry.

For example, if you have a group called "green\_bacteria" and you always
want their points to be in green use:

``"color_group": {"green_bacteria": "#00FF00"}``

or provided the fourth color of your color palette is green:

``"color_group": {"green_bacteria": "C3"}``

New output options
~~~~~~~~~~~~~~~~~~

In ``protocol.json``:

-  The consolidated ``results.xlsx`` is only exported if ``export_xls``
   is ``true`` (Default is ``false``).
-  Individual ``droplets/[xx].csv`` are only exported if
   ``individual_csv`` is ``true`` (Default is ``true``).

The file ``analysis/droplet_dynamics.csv`` is now leaner and sorted by
drop instead of run.

The new file ``analysis/signal_annotation.csv`` contains information
about the measures of all the peaks, even those which are not assignated
to a droplet or were dropped during the quality control.

New in 0.2.25 (13th June 2017)
==============================

**Automatic coalescence detection**. When droplet size is available
(i.e. two optical blocks are in use), the pipeline can try to
automatically detect and correct coalescence events.

This makes use of the variance in size of the drops and use it as a
"barcode" of the train. If a run contains less drop than the previous
one, the pipeline finds which droplets have to be removed from the
previous run to minimize the total variation in droplet size. This
simple heuristic works well on simple and double coalescence in hiccup
trains.

This new feature is activated by providing the option
``"automatic_coalescence_events":true`` in the protocol file. All
consecutive runs with a loss of less than ``"max_coalescence"`` (default
to 2) will be analyzed.

Manual input of the position of coalesced droplets (using
``"manual_coalescence_events"`` in the protocol file) is still possible
and will take priority over automatic detection when both are available.

It is now possible to provide multiple coalescence manually, by giving a
list of indices: ``[3,[11,93]]`` indicates that drops 11 and 93
coalesced in run 3 (you can still use ``[3,11]`` if only one coalescence
occurred).

New in 0.2.13 (24 May 2017)
===========================

1. The "hiccup" trains are now recognized by the pipeline, just keep the
   same template and put "visit\_mode": "hiccup" in your protocol file
   (the default is sequential". When in hiccup mode, the wells are
   visited by non-overlapping pairs alternating a droplet from each one,
   until ``droplet_per_well``\ (10 by default) droplets are done per
   well before moving on to the next pair
2. It is now able to measure the droplet size and speed when two
   photo-diodes are installed. You will find those measure in the
   regular output as ``speed`` and ``size`` if you have the relevant pmt
   activated in "active\_channels" (at least (pmt1 or pmt2) AND (pmt3 or
   pmt4)).
3. The way the mean signal of a droplet change slightly to correct the
   hardware physical offset between laser detection and fluorescence
   measure. We now measure the mean of the peak at the maximum of
   cross-correlation between the two signals. (The output of the
   pipeline contain the induced shift in CHANNEL\_shift).
4. The origin of the times is now the time of the first measured point
   in the raw signal. Unless there is a LogFile.txt and a Proc000.csv
   within the experiment folder, in this case, the pipeline will parse
   those to set the origins of time at the beginning of the train
   generation.
5. Added an option to hide the empty droplets in the dynamics plot of
   the Poisson plugin. Just set ``plugin.poisson.plot_empty_drops`` to
   false.

New in 0.2 (10 Apr 2017)
========================

-  Plugins
