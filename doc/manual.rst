User's manual
=============

.. toctree::
   :maxdepth: 1

Installation and update
------------------------

Stable version
~~~~~~~~~~~~~~

Stable version of ``dropsignal`` (starting with 0.4) are uploaded on
the Python Package Index (PyPi), and can be installed using ``pip``:

::

    pip install dropsignal


Obtaining the development version:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

If you have Python 3 along with the ``pip``, installing or upgrading to
the current development version of ``dropsignal`` should be as easy as:

::

    pip install https://gitlab.com/evomachine/dropsignal/repository/archive.zip?ref=master

If you are confused about the dependencies, the
`Anaconda <https://www.continuum.io/downloads>`__ python distribution
should get you started by installing Python 3 and a large collection of
scientific packages (including the ones required to run ``dropsignal``).

To use dropsignal with Anaconda you just have to run same command in
your "Anaconda Prompt":

::

    pip install https://gitlab.com/evomachine/dropsignal/repository/archive.zip?ref=master

If dropsignal is already installed, you can use the following command to
upgrade it (and only it):

::

    pip install --upgrade --no-deps https://gitlab.com/evomachine/dropsignal/repository/archive.zip?ref=master

On Windows this will install dropsignal GUI in
``C:\Anaconda3\Scripts\dropsignal_gui.exe`` (it is probably a good idea
to make a shortcut) and give you access to the other commands from the
Anaconda Prompt. On proper OS (any Linux distribution, really) you will
have access to all dropsignal commands from the terminal.

Note that the previous command will also upgrade dropsignal to the last
bleeding edge development version.

Start an analysis
-----------------

Graphical interface
~~~~~~~~~~~~~~~~~~~

Use the command ``dropsignal_gui``. Select the folder containing the
raw data with the ``Browse`` button. Start the analysis with the
``Start`` button and open the results with the ``Serve`` button.

Web interface
~~~~~~~~~~~~~

(New in 0.3.30)

Use the ``dropsignal_web --browser`` command. Select the folder to
scan with the ``scan`` field. You can edit the protocol, start
analysis and open results from the list.

Command line interface
~~~~~~~~~~~~~~~~~~~~~~

Once installed, the pipeline expose the ``dropsignal`` command.

::

    usage: dropsignal [-h] [--plot] [--pname PLUGIN] [--noraw] [--rebuild]
		      {pipeline,run,export_static,protocol,plugin,serve} PATH

    positional arguments:
      {pipeline,run,export_static,protocol,plugin}
			    Command. See below for a description.
      PATH                  Raw data location.

    optional arguments:
      -h, --help            show this help message and exit
      --plot                export raw signal plots (long).
      --pname PLUGIN        Name of the plugin. Installed plugins:
			    ('contamination_patterns', 'curve_fitting',
			    'hclustering', 'image_mosaic', 'poisson',
			    'train_heatmap')
      --noraw               do not use raw signal.
      --rebuild             try to autocorrect missing droplets (Experimental).

    Available commands:
    - pipeline: Process of the experiment in PATH.
    - run: Signal processing of the run folder in PATH.
    - plugin: Execute the plugin given by --pname PLUGIN in PATH (must have run pipeline first).
    - protocol: Export an example protocol file in PATH.
    - serve: Start to serve the result on localhost.
    - export_static: Export HTML static file in PATH.


The ``pipeline`` command
~~~~~~~~~~~~~~~~~~~~~~~~

This command is used to perform the full data analysis pipeline on an
experiment.

Input:
^^^^^^

The script will optionally look for annotations and protocol data in
``PATH/protocol.json`` and ``PATH/template.csv``.

You can use the ``dropsignal protocol PATH`` command to create an
example protocol file filed with default values and a short
documentation.

-  In ``template.csv`` with columns ``Well`` (string), ``Description``
   (string).
-  In ``protocol.json`` you may specify the number of droplets made by
   well (``droplets_per_well``, default is 10) and the order in which
   the wells are visited (default is ``snake90`` which correspond to
   cycling through a 96 well plate but skipping the first and last three
   wells).

(**Advanced users:**) For a finer control you can avoid using the
``template.csv`` file and directly create a ``droplets`` entry in
``protocol.json`` It must contain a list of json objects corresponding
to droplet groups. Each object must have a ``label`` (string) and
``well`` (string) field and finally an ``id`` field which contains a
list of the droplets position corresponding to the group.

In an ``id`` field, you can specify ranges of droplets, for instance a
group composed of droplets 1, 2 and 10,11... up to 20 will be formatted
like: ``{id:[1,2,[10,20]]], label:"sample group", well:'A9'}``. If
``id`` is ``"default"`` all the droplets non associated to another group
will be added to this group.

Output:
^^^^^^^

The output is placed in an ``analysis/`` folder in the raw data folder
(``PATH``) and includes:

-  ``droplet.csv``: summary information about individual droplets.
-  ``runs.csv``: summary information about individual runs.
-  ``droplet_dynamics.csv``: all droplet dynamics.
-  ``signal_annotation.csv``: measurments of all the peaks detected in
   the raw data, even the ones that were not used in the end.
-  ``droplets/{0000}.csv``: dynamics from individual droplets (only if
   ``individual_csv`` is ``true`` in ``protocol.json``).
-  ``metadata.json``: information about the experiment, enriched from
   the ``protocol.json``.
-  ``results.xls``: spreadsheet consolidated output (only if
   ``export_xls`` is ``true`` in ``protocol.json``)
-  ``detect_peak*.png``: raw signal plot (only if ``--plot`` is used).
-  ``plugins/``: One subfolder per plugin. Contain the output of the
   plugin.
-  ``index.html``, ``runs.html``, ``images.html``,
   ``contamination.html``, ``plugins.html`` and ``static/``: web based
   visualization files.

Plugins
~~~~~~~

*New in version 0.2*

Plugins are additional analysis of data processed and curated by the
main dropsignal pipeline.

Plugins are hooked at the end of the pipeline. Use the
``enabled_plugin`` option in your protocol file to specify which plugins
should be run. By default only ``train_heatmap`` is enabled.

You can also apply them individually without rerunning the whole
pipeline again using the new button in the interface or the new command
``dropsignal plugin`` (you must have run the pipeline at least once on
the same folder before).

Plugins are configured in the protocol file. Take a look at the end of
the example protocol file to have the list of options.

Open analysis results
---------------------

Dropsignal offer an visual interface to explore the results of the
analysis.

Open the experiment with ``dropsignal serve <path>`` or
``dropsignal_web`` or ``dropsignal_gui``.

The user interface is organized in three tabs:

-  **Dynamics**: This page shows the reconstructed per droplet-dynamics.
   You can filter the data using the droplet selector (bottom) or the
   microplate sketch.
-  **Scatter** Scatter plots of the droplets dynamics
-  **Signal**: This shows the raw output of the machine, run by run.
   Note that "Backs" runs are flipped beforehand so you can actually
   compare them with "forths" runs.
-  **Plugins** Additional analysis are stored here. See the
   documentation to add your own, it is as simple as writing a python
   script.

|scatter_demo|

   .. |scatter_demo| image:: img/scatter_demo.gif
