dropsignal\.plugins\.contamination\_patterns package
====================================================

Module contents
---------------

.. automodule:: dropsignal.plugins.contamination_patterns
    :members:
    :undoc-members:
    :show-inheritance:
