dropsignal\.plugins package
===========================

Subpackages
-----------

.. toctree::

    dropsignal.plugins.contamination_patterns
    dropsignal.plugins.curve_fitting
    dropsignal.plugins.hclustering
    dropsignal.plugins.image_mosaic
    dropsignal.plugins.poisson
    dropsignal.plugins.train_heatmap

Module contents
---------------

.. automodule:: dropsignal.plugins
    :members:
    :undoc-members:
    :show-inheritance:
