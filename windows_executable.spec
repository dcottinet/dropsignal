# -*- mode: python -*-

block_cipher = None
from PyInstaller.utils.hooks import collect_data_files, eval_statement
dropsignal_files = collect_data_files( 'dropsignal', subdir=None, include_py_files=False )
import dropsignal.plugins
import dropsignal
import tempfile
import os
import pip

with tempfile.TemporaryDirectory() as dir:
        dropsignal_files = list(dropsignal_files) + [(os.path.join(dir,'VERSION'),'dropsignal')]
        print(dropsignal_files)

        with open(os.path.join(dir,'VERSION'),'w') as f:
                for p in pip.get_installed_distributions():
                        k,v =  str(p).strip().split()
                        if k=='dropsignal':
                                f.write(v)
        dropsignal_exe = os.path.join(os.path.dirname(dropsignal.__file__),'webgui.py')
        icon = os.path.join(os.path.dirname(dropsignal.__file__),'static','dropsignal.ico')
        plugins = ['dropsignal.plugins.'+x for x in dropsignal.plugins.INSTALLED]
        a = Analysis([dropsignal_exe],
                                 pathex=['C:\\'],
                                 binaries=[],
                                 datas=dropsignal_files,
                                 hiddenimports=plugins,
                                 hookspath=[],
                                 runtime_hooks=[],
                                 excludes=[],
                                 win_no_prefer_redirects=False,
                                 win_private_assemblies=False,
                                 cipher=block_cipher)
        pyz = PYZ(a.pure, a.zipped_data,
                                 cipher=block_cipher)
        exe = EXE(pyz,
                          a.scripts,
                          exclude_binaries=True,
                          name='dropsignal',
                          debug=False,
                          strip=False,
                          upx=True,
                          console=True,
                          icon=icon,
                          )
        coll = COLLECT(exe,
                                   a.binaries,
                                   a.zipfiles,
                                   a.datas,
                                   strip=False,
                                   upx=True,
                                   name='dropsignal')
