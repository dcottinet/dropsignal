"""aggregate.py
Tools to aggregate unevenly spaced timeseries.
"""
import pandas as pd

def aggregate_per_run(droplet_dyn, droplets, kind=('well', 'group')):
    """ Aggregate timeseries by `kind`, do the mean and standard deviation run-wise. """
    out = []
    merged = pd.merge(droplet_dyn, droplets.ix[:, kind],
                      left_on='id_exp', right_index=True).drop(['offset', 'id_run', 'id_exp'], 1)
    for key in kind:
        grouped = merged.groupby([key, 'run'])
        mean = grouped.mean().rename(columns=lambda x: x+'_mean')
        std = grouped.std().rename(columns=lambda x: x+'_std')
        out.append(pd.merge(mean, std, left_index=True, right_index=True).reset_index())
        out[-1].rename(columns={key:'key'}, inplace=True)
        out[-1]["kind"] = key
    return pd.concat(out)
