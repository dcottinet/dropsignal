"""Pipeline.py
This file contains the logic to apply the full pipeline.
It also contains the entry point for the CLI.
"""
import os
import shutil
import logging
import datetime
import argparse
from glob import glob
from functools import partial

import pandas as pd
import cherrypy
import numpy as np

from dropsignal import __version__
import dropsignal.plots
import dropsignal.export
import dropsignal.processing
import dropsignal.quality
import dropsignal.rebuild
import dropsignal.protocol
import dropsignal.extract
import dropsignal.aggregate
import dropsignal.processing_binraw
import dropsignal.plugins
import dropsignal.index
import dropsignal.webgui

logger = logging.getLogger('dropsignal')
logger.setLevel(logging.DEBUG)
logger.propagate = False

def pipeline(path, wipe_previous=False):
    """ Run the complete pipeline on a folder"""
    # Check input folder
    if not glob(os.path.join(path, '*.raw')):
        raise ValueError("No *.raw file found. Are you sure this is the right path ?")

    logger.info("="*30+" PIPELINE START [v{}]".format(__version__) + "="*30)

    # Loading protocol data
    logger.info("~~~ Loading protocol ~~~")
    ppath = {'protocol_path':os.path.join(path, 'protocol.json'),
             'template_path':os.path.join(path, 'template.csv')}
    protocol, template = dropsignal.protocol.loader(**ppath)

    # Output folder configuration.
    if wipe_previous:
        shutil.rmtree(protocol["output_path"])
    if not os.path.exists(protocol["output_path"]):
        os.mkdir(protocol["output_path"])
    dropsignal.export.metadata(protocol, protocol["output_path"])

    # Setting up the logger
    file_handler = logging.FileHandler(os.path.join(protocol["output_path"], 'pipeline_log.txt'))
    file_handler.setLevel(logging.DEBUG)
    formatter = logging.Formatter("%(asctime)s %(levelname)s: %(message)s", "%m/%d/%Y %I:%M:%S")
    file_handler.setFormatter(formatter)
    logger.addHandler(file_handler)


    # === Find all the files. === #
    logger.info("~~~ File discovery ~~~")

    records = dropsignal.processing_binraw.discover(path)
    protocol["time_first"] = records['time'].min()
    protocol["duration"] = records['time'].max() - records['time'].min()
    protocol["time_last"] = records['time'].max()
    protocol["runs"] = records['run'].nunique()

    logger.info('{} runs discovered.'.format(protocol["runs"]))

    # === Peak detection and Integration === #
    logger.info("~~~ Signal Processing ~~~")

    process_param = {
        'window': protocol["window"],
        'window_size': protocol["window_size"],
        'threshold': protocol["detection_threshold"],
        'pmt': protocol['active_channels'],
        'channel_names': protocol['channels_names'],
        'output_path': protocol["output_path"],
        'alignment_correction_channels': protocol['correct_alignment_with_detection'],
        'reference_ptd': protocol['reference_photodiode'],
        'advanced_params':protocol['advanced_detection_parameters']}

    runs, peaks, protocol['T0_offset'] = dropsignal.processing_binraw.process(records,
                                                                              **process_param)

    # Update protocol informations: number of peaks
    if "expected_droplets" not in protocol:
        protocol["expected_droplets"] = int(np.ceil(runs.peaks.median()))
        logger.warning(("The train length (expected_droplet) was not defined "
                        "in the protocol. Falling back to the median number of "
                        "detected droplets: {}.").format(protocol["expected_droplets"]))
    else:
        protocol["expected_droplets"] = int(protocol["expected_droplets"])

    # === Run quality check === #
    logger.info("~~~ Run annotation and quality check ~~~")
    qc_tests = [dropsignal.quality.qc_length,
                dropsignal.quality.qc_peak_nb,
                partial(dropsignal.quality.qc_coalescence, thres=protocol['max_coalescence'])]
    for function in qc_tests:
        function(runs=runs, data=peaks, protocol=protocol)

    # Mark as good the runs without a certain list of tags.
    # If no run fulfill the conditions, relax them by taking less tags.
    bad_tags = ["partial_train", "too_much_droplets",
                "dramatic_coalescence", "probable_misscount",
                "too_short"]
    runs['good'] = 0
    for i, tag in enumerate(bad_tags):
        for run, tags in zip(runs.id, runs.tags):
            bad = frozenset(bad_tags[i:]).intersection(frozenset(tags))
            kept = len(bad) == 0
            if kept:
                txt = "kept."
            else:
                txt = "discarded because of: {}.".format(", ".join(bad))
            if (not kept and run == 1
                and "too_much_droplets" in runs.loc[runs.id == 0, 'tags'].values[0]
                and "dramatic_coalescence" in tags):
                kept = True
                txt = 'exceptionnaly kept (Run 0 has too much droplets and dramatic_coalescence)'

            logger.debug('Run {:02} {:25} tags:({})'.format(run, txt, ", ".join(tags)))
            runs.loc[runs.id == run, 'good'] = kept
        if np.int_(runs.good.values).sum() > 1:
            protocol['bad_tags'] = bad_tags[i:]
            break
        else:
            logger.debug(("No good run if we consider runs tagged"
                          "'{}' as bad, dropping this tag.").format(tag))

    protocol['good_runs'] = runs.good.sum()
    protocol['droplets_nb'] = runs[runs.good].peaks.max()
    logger.info(("{} runs annotated, {} are tagged as good."
                 "(Tag marked as bad: {})").format(protocol['runs'],
                                                   protocol['good_runs'],
                                                   protocol['bad_tags']))

    coalescent_runs = [runs.id[k]
                       for k in runs.index
                       if 'coalescence_event' in frozenset(runs.tags[k])]
    protocol['coalescence_events'] = len(coalescent_runs)

    # === Reconstruct timeseries === #
    logger.info("~~~ Reconstruct the timeseries ~~~")
    def to_str(dictionnary):
        """String representation of coalescence dictionnaries"""
        return str([[k, v] for k, v in sorted(dictionnary.items(), key=lambda x:x[0])])

    # Automatic coalescence detection
    if len(coalescent_runs) and protocol['automatic_coalescence_events']:
        logger.info(("{} runs might contain a coalescence event."
                     "Trying to detect their position...").format(len(coalescent_runs)))
        coalescence_events = dropsignal.rebuild.coalescence_detection(coalescent_runs, peaks)
        logger.info('{} coalescence event(s) detected ({}).'.format(len(coalescence_events),
                                                                    to_str(coalescence_events)))
    else:
        coalescence_events = {}

    # Manual coalescence event input
    if len(protocol['manual_coalescence_events']):
        protocol_cev = dict(protocol['manual_coalescence_events'])
        logger.info(('{} coalescence event(s) loaded from protocol'
                     '({}).').format(len(protocol_cev), to_str(protocol_cev)))

        for run, coal_id in protocol_cev.items():
            if run == np.min(runs[runs.good]).id:
                logger.critical(('Manual coalescence at the first analyzed run'
                                 'is strongly discouraged. Use the template file'
                                 'to manage a bad train generation'))
            if run in coalescence_events:
                logger.info(('Manual coalescence event is overwriting'
                             'automatic detection for run {}').format(run))
            coalescence_events[run] = coal_id

    protocol['coalescence_event_list'] = []
    for run, indices in coalescence_events.items():
        try:
            for idx in indices:
                protocol['coalescence_event_list'].append((int(run), int(idx)))
        except TypeError:
            protocol['coalescence_event_list'].append((int(run), int(indices)))

    droplet_dyn = dropsignal.rebuild.droplet_dynamics(runs[runs.good].id,
                                                      peaks,
                                                      coalescence_events)
    droplet_dyn['absolute_time'] = droplet_dyn['time']
    if protocol['relative_times']:
        logger.debug('Using relative times as default time')
        droplet_dyn['time'] = droplet_dyn['relative_time']
    else:
        logger.debug('Using absolute times as default time')

    logger.debug("Individual dynamics rebuilt accross {} runs".format(runs.good.sum()))

    # === Droplets annotation === #
    logger.info("~~~ Droplet annotation and quality check ~~~")
    droplets = dropsignal.quality.annotate_droplets(droplet_dyn)
    droplets["well"] = [protocol["droplet_to_well"][x] for x in droplets.index]
    if "well" in template and "description" in template:
        droplets = pd.merge(droplets, template.loc[:, ["well", "description"]],
                            left_on="well", right_on='well', how="left")
    droplets.rename(columns={"description":"group"}, inplace=True)
    if 'expected_growth' not in droplets.columns:
        droplets['expected_growth'] = 1
    logger.debug("{} droplets annotated.".format(droplets.shape[0]))

    # === Aggregate === #
    logger.info('~~~ Aggregating timeseries ~~~')
    aggregated = dropsignal.aggregate.aggregate_per_run(droplet_dyn, droplets)

    # === Apply plugins ===#
    data = {"dynamics":droplet_dyn, "droplets":droplets, "runs":runs}
    for name in protocol["enabled_plugins"]:
        logger.debug("~~~ Plugin: {}~~~".format(name))
        dropsignal.plugins.apply_plugin(name, data, protocol)

    # === Export === #
    logger.info("~~~ Export ~~~")
    protocol["status"] = "OK"
    dropsignal.export.analysis_folder(runs, droplets, droplet_dyn, aggregated, protocol, protocol["output_path"])
    logger.info("~~~ END ~~~")

    # Detach the handler it stops writing in the the file.
    logger.handlers.pop(logger.handlers.index(file_handler))
    return path

def main():
    """ CLI entry point """
    description = ''' Extraction, conversion and visualization of experimental data from millifluidic droplet trains.

Most of the configuration is done using the protocol.json and
template.csv files. Use the `protocol` command to write example
file.'''

    parser = argparse.ArgumentParser(description=description,
                                     formatter_class=argparse.RawDescriptionHelpFormatter,
                                     epilog=
                                     '''Available commands:
- pipeline: Process of the experiment in PATH.
- run: Signal processing of the run folder in PATH.
- plugin: Execute the plugin given by --pname PLUGIN in PATH (must have run pipeline first).
- protocol: Export an example protocol file in PATH.
- index: Export an index page that list analysed experiments in PATH.
- serve: Start to serve the result on localhost.
- export_static: Export HTML static file in PATH.

Report bugs at https://gitlab.com/evomachine/dropsignal/issues.
You are using version {}
                                     '''.format(__version__))
    parser.add_argument('command',
                        choices=['pipeline', 'run', 'export_static',
                                 'protocol', 'plugin', 'index', 'serve'],
                        type=str,
                        help='Command. See below for a description.')
    parser.add_argument('path', metavar='PATH', type=str,
                        default='.',
                        help='Raw data location.')
    parser.add_argument('--plot',
                        action='store_true',
                        help="(Deprecated)")
    parser.add_argument('--pname', metavar="PLUGIN", type=str,
                        help=("Name of the plugin."
                              "Installed plugins: "
                              "{}").format(dropsignal.plugins.INSTALLED))
    parser.add_argument('--port', default=8080, help="(serve) Server port")
    parser.add_argument('--public', action='store_true', help="(serve) Serve on public IP")
    parser.add_argument('--browser', action='store_true', help="(serve) Open browser")

    args = parser.parse_args()

    console_handler = logging.StreamHandler()
    console_handler.setLevel(logging.DEBUG)
    console_handler.setFormatter(logging.Formatter('%(levelname)-7s  %(message)s'))
    logger.addHandler(console_handler)

    if args.command == 'export_static':
        analysis = os.path.join(args.path, 'analysis')
        if os.path.exists(analysis):
            path = analysis
        else:
            path = args.path
        dropsignal.export.static(path, overwrite=True)
        return 0
    elif args.command == 'protocol':
        example_protocol = os.path.join(args.path, 'protocol.json')
        example_template = os.path.join(args.path, 'template.csv')
        if os.path.exists(example_protocol):
            logger.warning('{} exists already'.format(example_protocol))
            example_protocol = os.path.join(args.path, 'protocol_example.json')
        if os.path.exists(example_template):
            logger.warning('{} exists already'.format(example_template))
            example_template = os.path.join(args.path, 'template_example.csv')
        open(example_protocol, 'w').write(dropsignal.protocol.example_protocol())
        logger.info("Example protocol saved in {}".format(example_protocol))
        open(example_template, 'w').write(dropsignal.protocol.example_template())
        logger.info("Example template saved in {}".format(example_template))

    elif args.command == 'plugin':
        try:
            dropsignal.plugins.load_data_and_apply(args.path, args.pname)
        except Exception as ex:
            logger.exception("Error in plugin: {}".format(ex))

    elif args.command == 'index':
        try:
            dropsignal.index.discover_and_index(args.path)
        except Exception as ex:
            logger.exception("Error in indexing: {}".format(ex))

    elif args.command == 'pipeline':
        try:
            pipeline(args.path)
        except Exception as ex:
            logger.exception("Error in pipeline: {}".format(ex))

    if args.command == 'serve':
        try:
            if not args.public:
                logger.warning('To access the server from another computer use the --public option')
            path = os.path.dirname(args.path)
            app = dropsignal.webgui.Experiment(os.path.expanduser(path))
            dropsignal.webgui.start_server(args.port, args.browser, args.public, False)
            cherrypy.tree.mount(app, '/', app.conf)
            while True:
                pass
        except Exception as ex:
            logger.exception("Error in serve: {}".format(ex))

    elif args.command == 'run':
        logger.warning('dropsignal run is deprecated and will be removed soon')
        single_run(args.path, args.plot)

def single_run(path, plot=True):
    ppath = os.path.join(os.path.dirname(path), 'protocol.json')
    protocol, _ = dropsignal.protocol.loader(protocol_path=ppath)
    if os.path.basename(path).split('.')[-1] == 'raw':
        _, peaks = dropsignal.processing_binraw.singlerun(path,
                                                          window=protocol['window'],
                                                          window_size=protocol['window_size'],
                                                          threshold=protocol['detection_threshold'],
                                                          pmt=protocol['active_channels'],
                                                          plot=plot)
        csv_file = "{}.csv".format('.'.join(os.path.basename(path).split('.')[:-1]))
        dropsignal.rebuild.concatenate_channels(peaks).set_index("id_run").to_csv(csv_file)
