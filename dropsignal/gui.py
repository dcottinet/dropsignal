"""gui.py -- Graphical interface for computer illiterates
dropsignal - Guilhem Doulcier 2018 AGPL3+
"""
import os
import logging
import socket
import webbrowser

import tkinter as tk
import tkinter.filedialog as filedialog
import cherrypy

import dropsignal.pipeline
import dropsignal.protocol
import dropsignal.plugins
import dropsignal.webgui

from dropsignal import __version__
logger = logging.getLogger('dropsignal')

WELCOME_MSG = '''Welcome in dropsignal v{}.
To proceed:
- select an experiment with the [Browse] button
- (optional) edit the protocol and template files using the [Edit]s buttons.
- start the pipeline with [Start]
If you would like to open a previously analysed dataset, use the button [Serve] instead of [Start].
'''.format(__version__)

def file_editor_creator(path, root, default):
    '''Create a window to edit a text file, if the file does not exist
    fill it with default.

    Args:
        path: (str) path of the file to edit
        root: (tkinter) root window
        default: (str) default file content

    Return the window object and the text field object.
    '''
    # Init
    window = tk.Toplevel(root)
    text = tk.Text(window)
    path_var = tk.StringVar(value=path)
    filename = tk.Label(window, textvariable=path_var)
    window.wm_title("Dropsignal - Edit:{}".format(path))

    # Load the text
    if os.path.exists(path):
        logger.info("Loading {}".format(path))
        with open(path, 'r') as file:
            text.insert(tk.END, file.read())
    else:
        logger.info("{} does not exists, loading an example file.".format(path))
        text.insert(tk.END, default)

    # Buttons definition
    def save():
        """Save the content of text in path and close the window"""
        with open(path, 'w') as file:
            file.write(text.get(1.0, tk.END))
        logger.info("{} saved".format(path))
        window.destroy()

    save_btn = tk.Button(window, text="Save", command=save)
    cancel_btn = tk.Button(window, text="Cancel", command=window.destroy)

    # Widget placement
    filename.grid(row=0, column=0, columnspan=2)
    text.grid(row=1, column=0, columnspan=3)
    save_btn.grid(row=2, column=1)
    cancel_btn.grid(row=2, column=2)
    return window, text

class WidgetLogger(logging.Handler):
    """A Logging handler to output in a tkinter text field, with colored messages."""
    def __init__(self, widget):
        logging.Handler.__init__(self)
        self.setLevel(logging.DEBUG)
        self.widget = widget
        self.widget.config(state='disabled')
        self.widget.tag_config("INFO", foreground="black")
        self.widget.tag_config("DEBUG", foreground="grey")
        self.widget.tag_config("WARNING", foreground="orange")
        self.widget.tag_config("ERROR", foreground="red")
        self.widget.tag_config("CRITICAL", foreground="red", underline=1)
    def emit(self, record):
        self.widget.config(state='normal')
        self.widget.insert(tk.END, self.format(record) + '\n', record.levelname)
        self.widget.see(tk.END)  # Scroll to the bottom
        self.widget.config(state='disabled')
        self.widget.update()

class Application(tk.Frame):
    """ The main window """
    def __init__(self, master=None):
        # Setup
        tk.Frame.__init__(self, master)
        self.master = master
        self.master.wm_title("Dropsignal")
        self.path = tk.StringVar('')
        self.path.set(os.getcwd())
        self.pack()
        self.app = None

        # Buttons
        browse_btn = tk.Button(self, text="Browse", command=self.pick_dir)
        filename = tk.Label(self, textvariable=self.path)
        self.start_btn = tk.Button(self, text="Start", command=self.start)
        detect_btn = tk.Button(self, text="Quick detect on one run", command=self.detect_one_run)
        self.serve_btn = tk.Button(self, text="Serve", command=self.serve)
        quit_btn = tk.Button(self, text="Quit", command=self.quit)
        edit_protocol_btn = tk.Button(self, text="Edit Protocol", command=self.edit_protocol)
        edit_template_btn = tk.Button(self, text="Edit Template", command=self.edit_template)
        self.buttons = [browse_btn, self.start_btn, edit_protocol_btn, edit_template_btn, detect_btn, self.serve_btn]

        ### Option buttons ###
        frame = tk.Frame(self, height=2, bd=1, relief=tk.SUNKEN)
        self.webbrowser = tk.IntVar(value=1)
        self.webbrowser_check = tk.Checkbutton(frame, text="Open results", variable=self.webbrowser)

        self.from_raw = tk.IntVar(value=1)
        self.from_raw_check = tk.Checkbutton(frame, text="From Raw Data", variable=self.from_raw)

        self.plots = tk.IntVar()
        self.plots_check = tk.Checkbutton(frame, text="Signal Plots", variable=self.plots)

        ### Plugin Apply ###
        frame_plugin = tk.Frame(self, height=2, bd=1, relief=tk.SUNKEN)
        self.plugin_to_apply = tk.StringVar(value=dropsignal.plugins.INSTALLED[0])
        drop_plugin = tk.OptionMenu(frame_plugin, self.plugin_to_apply, *dropsignal.plugins.INSTALLED)
        drop_plugin.grid(row=1,column=1)
        btn_plugin = tk.Button(frame_plugin, text="Apply Plugin", command=self.apply_plugin)
        btn_plugin.grid(row=1,column=2)
        self.buttons.append(btn_plugin)

        # Attach the Widget handler.
        self.text = tk.Text(self)
        self.text.tag_config("status", background="#dff0d8")
        self.text.insert(tk.END, WELCOME_MSG, "status")
        self.logging_handler = WidgetLogger(self.text)
        logger.addHandler(self.logging_handler)

        # Widget placement
        browse_btn.grid(row=0, sticky="W")
        filename.grid(row=0, sticky="E")

        frame.grid(row=1)
        self.from_raw_check.grid(row=1, column=1)
        self.webbrowser_check.grid(row=1, column=3)
        self.plots_check.grid(row=1, column=4)

        quit_btn.grid(row=5,  sticky="E")
        self.start_btn.grid(row=5, sticky="W")
        self.serve_btn.grid(row=5)

        self.text.grid(row=2)
        frame_plugin.grid(row=3, sticky="E")
        detect_btn.grid(row=4, sticky="E")
        edit_protocol_btn.grid(row=3, sticky="W")
        edit_template_btn.grid(row=4, sticky="W")

    def edit_protocol(self):
        """Edit the protocol.json file in a new window"""
        file_editor_creator(path=os.path.join(self.path.get(), "protocol.json"),
                            root=self,
                            default=dropsignal.protocol.example_protocol())


    def edit_template(self):
        """Edit the template.csv file in a new window"""
        window, template_text = file_editor_creator(path=os.path.join(self.path.get(), "template.csv"),
                                                    root=self,
                                                    default=dropsignal.protocol.example_template())

        # Example loader
        frame = tk.Frame(window, height=2, bd=1, relief=tk.SUNKEN)
        order = tk.StringVar(value=dropsignal.protocol.PROTOCOL_DOC['well_order']['default'])
        drop = tk.OptionMenu(frame, order, *dropsignal.protocol.WELL_ORDER.keys())
        def load_example():
            """ Replace the content of the text widget by the required example """
            template_text.delete(1.0, tk.END)
            template_text.insert(tk.END, dropsignal.protocol.example_template(dropsignal.protocol.WELL_ORDER[order.get()]))
        button = tk.Button(frame, text="Load example", command=load_example)
        drop.grid(row=0, column=0)
        button.grid(row=0, column=1)
        frame.grid(row=0, column=2)

    def pick_dir(self):
        """Select a directory"""
        newpath = filedialog.askdirectory(title='Pick a directory')
        if len(newpath):
            self.path.set(newpath)
            logger.info("Folder selected: {}".format(self.path.get()))

    def serve(self):

        if self.app is None:
            self.app = dropsignal.webgui.Experiment(os.path.expanduser(self.path.get()))
            cherrypy.tree.mount(self.app,'/',self.app.conf)

            self.start_btn.config(state='disabled')
            self.serve_btn['text'] = 'Stop Serving'
            self.start_btn['text'] = 'Serving'

            if self.webbrowser.get():
                webbrowser.open('http://localhost:{}'.format(PORT))
            msg = '\nServing the data on http://localhost:{}\n'.format(PORT)
        else:

            self.app.teardown()
            del self.app
            self.app = None

            self.start_btn.config(state='normal')
            self.serve_btn['text'] = 'Serve'
            self.start_btn['text'] = 'Start'

            msg = '\nStopped the web service\n'
        self.text.config(state='normal')
        self.text.insert(tk.END, msg, 'status')
        self.text.update()

    def detect_one_run(self):
        path = filedialog.askopenfilename(title='Pick a file', filetypes=[('Raw signal files', '*.raw')])
        for btn in self.buttons:
            btn.config(state='disabled')
        try:
            dropsignal.pipeline.single_run(path, plot=True)
        except Exception as ex:
            logger.exception("Error in pipeline: {}".format(ex))
        else:
            logger.info("Done.")
            if self.webbrowser.get():
                webbrowser.open(path.replace('.raw','.png'))
        for btn in self.buttons:
            btn.config(state='normal')

    def apply_plugin(self):
        for btn in self.buttons:
            btn.config(state='disabled')
        try:
            dropsignal.plugins.load_data_and_apply(self.path.get(), self.plugin_to_apply.get())
        except Exception as ex:
            logger.exception("Error in plugin: {}".format(ex))
        finally:
            for btn in self.buttons:
                btn.config(state='normal')
        self.text.update()

    def start(self):
        """ Start the analysis with the pipeline and serve the results"""
        for btn in self.buttons:
            btn.config(state='disabled')
        try:
            dropsignal.pipeline.pipeline(self.path.get())
        except Exception as ex:
            logger.exception("Error in pipeline: {}".format(ex))
        else:
            for btn in self.buttons:
                btn.config(state='normal')
            # End message
            self.text.config(state='normal')
            self.text.insert(tk.END,
                             'Processing over ! You can see the result in the analysis folder',
                             'status')
            self.text.see(tk.END)
            self.text.config(state='disabled')
            self.serve()
        self.text.update()

    def quit(self):
        cherrypy.engine.exit()
        self.master.destroy()

def get_open_port(preferred=8080):
    ''' Return a free and open port '''
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        try:
            # Check if the preferred port is free
            s.bind(('127.0.0.1',  preferred))
            s.listen(1)
            port = preferred
        except socket.error as e:
            # Get another
            s.bind(("",0))
            s.listen(1)
            port = s.getsockname()[1]
    return port

PORT = get_open_port()

def main():
    """Setup the logger & start the main loop"""
    logger.setLevel(logging.DEBUG)
    logger.propagate = False
    cherrypy.log.error_log.propagate = False
    cherrypy.log.access_log.propagate = False
    log = False
    public = False
    cherrypy.config.update({
        'server.socket_port': PORT,
        'server.socket_host': '0.0.0.0' if public else '127.0.0.1',
        'log.screen':log,
        'tools.staticdir.debug': log,
    })
    cherrypy.engine.start()
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    ch.setFormatter(logging.Formatter('%(levelname)-7s  %(message)s'))
    logger.addHandler(ch)
    logger.warning('dropsignal_gui is deprecated and will be removed soon. You should use dropsignal_web')
    root = tk.Tk()
    app = Application(master=root)
    app.mainloop()

if __name__ == "__main__":
    main()
