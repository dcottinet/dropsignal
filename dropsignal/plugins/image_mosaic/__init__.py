"""Images."""
import os
import logging
from dropsignal.plugins.image_mosaic.images import process_files

PARAMETERS = {"path_images":{"description":"Location of the pictures",
                                  "default": "IMG/"}}

logger = logging.getLogger('dropsignal.plugins.image_mosaic')

def main(data, protocol, parameters):
    if os.path.isabs(parameters['path_images']):
        path_img = parameters['path_images']
    else:
        path_img = os.path.join(protocol['path'], parameters['path_images'])

    if os.path.exists(path_img):
        logger.info("Images folder found.")
        try:
            if not os.path.exists(path_img_out):
                os.mkdir(path_img_out)
            img = process_files(path_img, output_path=parameters["export_path"])
        except Exception as ex:
            logger.exception("Image analysis failed: {}".format(ex))
            img = None
    else:
        img = None
        logger.info("No image folder")

    if img is not None:
        # A file that gives the informations about the pictures.
        img.set_index(["id_run", "run"]).to_csv(os.path.join(parameters["export_path"], 'pictures.csv'))

        # Webpage
        html = ""
        for _, im in img.iterrows:
            html += '<div><h3>Run: {}</h3><img src="{}"/></div>'.format(im.run, im.path)
        with open(os.path.join(parameters["export_path"], 'index.html'), 'w') as fi:
            fi.write(html)
