"""Hierarchical clustering

Linear interpolation of the dynamics, building of a distance matrix,
and hierarchical clustering.

Author: Guilhem Doulcier

"""
import os
import logging
import inspect
from functools import partial
from itertools import combinations
from collections import defaultdict

import numpy as np
import scipy.cluster.hierarchy as sch
import pandas as pd
import matplotlib
matplotlib.use('agg')
from matplotlib.ticker import FuncFormatter
import matplotlib.pyplot as plt

logger = logging.getLogger('dropsignal.plugins.clustering')
PARAMETERS = {"channel":{"description":"Channel used to cluster.",
                         "default": "fluo_1_max"},
              "method":{"description":"Hierarchical clustering method, see scipy.cluster.hierarchy.linkage documentation.",
                        "default":"weighted"},
              "distance":{"description": ("Method used to compute the distance. Available: 'euclidian', 'mx_crosscorr'"
                                          "(maximum of cross-correlation) and 'delay' (position of the maximum of cross-correlation)."), "default":"delay"},
              "max_cluster":{"description":"Maximum number of clusters","default":12}}

def main(data, protocol, parameters):

    logger.debug('Linear interpolation...')
    linint = linear_interpolation(data["dynamics"], parameters["channel"])
    logger.debug('Compute distance...')
    D,labels = distance_matrix(linint, parameters["distance"])
    logger.debug('Clustering...')
    clustering = sch.linkage(D, method='weighted')
    clusters={}
    for i in range(2,parameters['max_cluster']+1):
        clusters[i] = sch.fcluster(clustering, i, criterion='maxclust')
    clusters = pd.DataFrame(clusters)
    clusters['id_exp'] = labels
    data['droplets'].index.name = 'id_exp'
    clusters = pd.merge(clusters, data['droplets'].reset_index().loc[:,['id_exp','group']])
    clusters.set_index('id_exp', inplace=True)
    color_group = {gname:'C{}'.format(i%9) for i,gname in enumerate(clusters['group'].unique())}

    logger.debug('Plotting...')
    for i in range(2,parameters['max_cluster']+1):
        plot_dynamics(data['dynamics'], clustering, clusters, color_group, i, parameters['channel'], parameters['export_path'])

    plot_distance_matrix(D, clustering, parameters['max_cluster'], parameters['export_path'])

    html = '<h1>Hierarchical Clustering</h1>'
    html += '<p>All {0} dynamics where linearly interpolated, then a pairwise distance was computed using method: {1[distance]}. </p>'.format(D.shape[0],parameters)
    html += '<p>Hierarchical clustering was performed on channel {0[channel]}. Method: {0[method]}, Maximum depth: {0[max_cluster]}</p>'.format(parameters)
    html += '<a href="distance_matrix.png"> <img src="distance_matrix.png" style="max-width:100%"/></a>'
    for i in range(2, parameters['max_cluster']+1):
        html += '<div><a href="dynamics_{0}.png"><img src="dynamics_{0}.png"style="max-width:100%"/></a>'.format(i)
    html += clusters.to_html()
    html += '<a href="clusters.csv">Download</a>'
    clusters.to_csv(os.path.join(parameters["export_path"], 'clusters.csv'))
    with open(os.path.join(parameters["export_path"], 'index.html'), 'w') as file:
        file.write(html)

def plot_dynamics(dynamics,  clustering, clusters, color_group, depth, channel,path):
    fig = plt.figure(figsize=(depth*8,5))
    axes = [plt.subplot(1,depth+2,j) for j in range(3,depth+3)]
    ax_dendo = plt.subplot(1,depth+2,1)
    ax_prop = plt.subplot(1,depth+2,2)
    plot_proportions(clusters, depth, color_group, ax_prop)

    ax_dendo.set_title('Depth={}'.format(depth))
    sch.dendrogram(
        clustering,
        truncate_mode='lastp',
        p=depth,
        ax=ax_dendo,
        leaf_rotation=90.,
        leaf_font_size=12.,
        show_contracted=True,
    )
    count = clusters.groupby(depth).count().values[:,0]
    formatter = FuncFormatter(tick_fmt)

    for j,ax in enumerate(axes,1):
        ax.set_title('Cluster i_{} ({} drops)'.format(j,count[j-1]))
        ax.set_xlabel('time (hms)')
        ax.set_ylabel(channel)
        ax.xaxis.set_major_formatter(formatter)

    for idx, df in dynamics.groupby('id_exp'):
        ax = axes[clusters.loc[idx,depth]-1]
        ax.plot(df['time'], df[channel], color=color_group[clusters.loc[idx,'group']])
    plt.savefig(os.path.join(path,'dynamics_{}.png'.format(depth)),bbox_inches='tight')
    plt.close(fig)

def linear_interpolation(dynamics,channel):
    linint = {}
    tmax = dynamics.time.max()
    tmin = dynamics.time.min()
    tinterp = np.linspace(tmin,tmax,2*dynamics.run.nunique())
    for idx,df in dynamics.groupby('id_exp'):
        linint[idx] = np.interp(tinterp, df['time'],df[channel])
    return linint

def distance_matrix(linint, distance = 'euclidian'):
    N = len(linint.keys())
    keys = list(enumerate(sorted(list(linint.keys()))))
    DD = np.zeros((N,N))
    if distance=='euclidian':
        dist = lambda x,y: np.linalg.norm(x-y)
    elif distance=="mx_crosscorr":
        dist = lambda x,y: np.max(np.correlate(x, y, mode='full'))
    elif distance=="delay":
        dist = lambda x,y: (np.argmax(np.correlate(x, y, mode='full'))-len(y)-1)
    for ((i,a),(j,b)) in combinations(keys,2):
        DD[i,j] = dist(linint[a], linint[b])
        DD[j,i] = DD[i,j]
    return DD, dict(keys)

def plot_distance_matrix(D,clustering, depth, path):

    # Figure configuration
    fig = plt.figure(figsize=(8,8))
    axmatrix = fig.add_axes([0.5,0.1,0.6,0.6])
    ax1 = fig.add_axes([0.09,0.1,0.4,0.6])
    ax2 = fig.add_axes([0.5,0.71,0.6,0.2])
    ax2.set_title('Distance matrix')
    # Plot dendograms
    sch.dendrogram(clustering, orientation='top',ax=ax2, truncate_mode='lastp',  p=depth)
    Z1 = sch.dendrogram(clustering, orientation='right',ax=ax1)

    ax1.set_xticks([])
    ax1.set_yticks([])
    ax2.set_xticks([])
    ax2.set_yticks([])

    # Reorder the lines and columns of the matrix.
    idx = Z1['leaves']
    D = D[idx,:]
    D = D[:,idx]

    # Plot matrix
    im = axmatrix.imshow(D, aspect='auto', origin='lower')
    axmatrix.set_xticks([])
    axmatrix.set_yticks([])

    plt.savefig(os.path.join(path,'distance_matrix.png'),bbox_inches='tight')
    plt.close(fig)

def tick_fmt(d,pos):
    h = d//60
    return "{}:{}:{}".format(int(h//60),int(h%60),int(d%60))

def plot_proportions(clusters, depth, group_colors, ax):
    bottoms = defaultdict(lambda: 0)
    labn = defaultdict(lambda: 0)
    count = clusters.groupby(depth).count().group
    for (clust,group), v in clusters.groupby([depth,'group']):
        height =  v.shape[0]/count[clust]
        plt.bar(clust,
                height,
                bottom=bottoms[clust],
                color=group_colors[group],
                label=group if not labn[group] else '__nolegend__')
        labn[group] += 1
        bottoms[clust] += height
        plt.sca(ax)
        plt.legend()
        plt.xticks(range(1,depth+1))
        plt.xlim(0,depth+2)
