"""Curve fitting.

Will try to fit several functional forms (linear, piecewise linear,
exponential, sigmoid) on the time series. Output the parameters, the
error, and the best fit.

Author: Guilhem Doulcier.
"""
import os
import logging
import inspect
from functools import partial

import numpy as np
import scipy.optimize as opt
import pandas as pd
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt

logger = logging.getLogger('dropsignal.plugins.curve_fitting')
PARAMETERS = {"channel":{"description":"Channel used to fit curves.",
                         "default": "fluo_1_mean"}}


def exponential(t, r, d, x0):
    """Exponential model

    Args:
       r: Growth rate
       d: value at t0
       x0: offset
    """
    return d*np.exp(r*t)+x0

def affine(t, x0, r):
    """Affine model

    Args:
       x0: intercept
       r: slope
    """
    return r*t+x0

def piecewise_affine_2(t, x0, r, d, r2):
    """Piecewise affine function (2 pieces)

    Args:
       x0: intercept 1
       r: slope 1
       d: breackpoint
       r2: slope 2
    """
    return (x0+r*t)*(t < d)+(t >= d)*(r2*(t-d)+(x0+r*d))

def gompertz(t, K, b, c, offset):
    """Gompertz function (sigmoïd)

    Args:
       K: asymptote (carrying capacity)
       b: time translation (lag)
       c: growth rate
       offset: y-translation
    """
    return K*np.exp(-b*np.exp(-t*c))+offset

def main(data, protocol, parameters):
    logger.debug('Curve fit')
    output = curve_fitting(data['dynamics'], parameters["channel"])

    logger.debug('Exports')
    html = "<h1> Curve Fitting </h1>"
    html += "<h2> Models </h2>"
    for model in [affine, piecewise_affine_2, exponential, gompertz]:
        html += "<strong> {} </strong>".format(model.__name__)
        html += "<pre> {} </pre>".format(inspect.getsource(model))
    html += "<h2> Results </h2>"
    html += "<pre>"+str(output.groupby('best_fit').count()['name'])+'</pre>'
    html += output.set_index('name').to_html()
    html += '<a href="curve_fitting.csv">Download</a>'
    output.set_index('name').to_csv(os.path.join(parameters["export_path"], 'curve_fitting.csv'))
    with open(os.path.join(parameters["export_path"], 'index.html'), 'w') as file:
        file.write(html)

def curve_fitting(data,channel):
    output = []
    for k,d in data.groupby('id_exp'):

        # Rough estimate of the parameters to initialize the optimisation algorithm
        r0 = (d[channel].values[-1]/d[channel].values[0])/(d.time.values.max()-d.time.values.min())
        d0 = 0.5*(d.time.values.max()-d.time.values.min()) + d.time.values.min()
        x0 = d[channel].values[0]
        xmax = d[channel].max()
        r0exp = np.log(d[channel].values[-1]/d[channel].values[0])/(d.time.values.max()-d.time.values.min())

        models = [
            {'name': 'affine',
             'func':affine,
             'p0':(x0, r0)
            },
            {'name': 'piecewise_affine_2',
             'func': piecewise_affine_2,
             'p0':(x0, 0, d0, r0)
            },
            {'name': 'exponential',
             'func': exponential,
             'p0':(r0exp, 0, 0)
            },
            {'name': 'gompertz',
             'func': gompertz,
             'p0':(xmax, 0, r0exp, 0)
            }
        ]
        output.append({'name':k})

        # Actual curve fitting
        error = []
        for i, m in enumerate(models):
            try:
                p, c = opt.curve_fit(m['func'], d.time.values, d[channel].values,
                                     p0=m['p0'])
                error.append(np.sum(d[channel].values-m['func'](d.time.values, *p)**2))
            except (Exception, RuntimeError):
                p = []
                error.append(np.inf)
            output[-1][m['name']+'_error'] = error[i]
            output[-1][m['name']+'_param'] = p

        # Store the bese fit
        output[-1]['best_fit']= models[np.argmin(error)]['name']

    # Convert to dataframe
    return pd.DataFrame(output)
