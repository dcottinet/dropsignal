"""Train Heatmap - Draw heatmaps of the signal."""
import os
import logging
import matplotlib
matplotlib.use('agg')
import numpy as np
import matplotlib.pyplot as plt

logger = logging.getLogger('dropsignal.plugins.train_heatmap')
PARAMETERS = {"channel_suffixes":{"description":"Pattern used to choose which columns are plotted",
                                  "default": ("max", "value", "mean")}}

def main(data, protocol, parameters):
    files = []
    for chan in data["dynamics"].columns:
        if "_" in chan and chan.split("_")[-1] in parameters['channel_suffixes']:
            try:
                files.append(train_heatmap(data["dynamics"], chan, outdir=parameters['export_path']))
            except Exception as ex:
                logger.exception("Error in plot {} : {}".format(chan, ex))
    html = "<h1> Train Heatmap </h1>"
    for fi in files:
        html += '<div class="plot"><img src="{}"/></div>'.format(os.path.basename(fi))
    with open(os.path.join(parameters["export_path"], 'index.html'), 'w') as file:
        file.write(html)

def train_heatmap(dynamics, column, outdir):
    """Draw the heatmap of the dynamics"""
    fig = plt.figure(figsize=(20, 15))
    ax = plt.gca()
    if 'viridis' in plt.colormaps():
        cmap = 'viridis'
    else:
        cmap = 'bone'
        logger.warning('Viridis colormap not found. You should update matplotlib.')

    pivoted = dynamics.loc[:, ["run", "id_exp", column]].pivot(columns="id_exp", values=column, index="run")
    im = ax.imshow(pivoted.values, cmap=cmap, aspect='auto', interpolation='none')
    ax.set_xlabel("Droplet number")
    ax.set_ylabel("Run")
    ax.set_title(column)
    ax.set_xticks(np.arange(0, dynamics.id_exp.max(), 50))
    axcolor = fig.add_axes([0.91, 0.2, 0.01, 0.6])
    plt.colorbar(im, cax=axcolor)
    filename = os.path.join(outdir, 'heatmap_{}.png').format(column)
    fig.savefig(filename, dpi=70, bbox_to_inches='tight')
    return filename
