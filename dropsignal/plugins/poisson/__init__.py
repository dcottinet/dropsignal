"""Poisson module

Will detect empty droplets, compute the expected number of droplets
that would have 1 cell and "2 cells or more" if the dilution was
following a Poisson Process. In order to study lag, it will also
compute the the distribution of times to reach a given threshold.

Author: Guilhem Doulcier
"""
import os
import glob
import logging

import pandas as pd
import numpy as np
from scipy.stats import gaussian_kde
import matplotlib
matplotlib.use('agg')
from matplotlib.ticker import FuncFormatter
import matplotlib.pyplot as plt

import dropsignal.quality

logger = logging.getLogger('dropsignal.plugins.poisson')

PARAMETERS = {"threshold":{"description":"Value above which the droplet is considered grown","default":1.0},
              "channel":{"description":"Channel used to detect empty droplets.","default":"fluo_2_max"},
              "plot_empty_drops":{"description":"If true, plots empty droplets dynamics.","default":True},
              "max_time":{"description":"Limit time to detect non-empty droplets (in minutes).", "default":600.0},
              "include_empty":{"description":"Include empty droplets in the time distribution estimation.", 'default':True},
              "restrict_to_groups":{"description":"List the group to analyse, if empty all groups will be treated", "default":tuple()}}

def main(data, protocol, parameters):
    """"""
    # convert to seconds.
    parameters['max_time'] *= 60
    parameters['max_time'] = min(data['dynamics']['time'].max(), parameters['max_time'])

    # Filter the data.
    data['droplets'].index.name = 'id_exp'
    if len(parameters['restrict_to_groups']):
        logger.debug('Analysing only groups: {}'.format(parameters['restrict_to_groups']))
        groups = frozenset(parameters['restrict_to_groups'])
        droplets = frozenset([row.id_exp
                    for k,row
                    in data['droplets'].reset_index().loc[:, ['id_exp', 'group']].iterrows()
                    if row.group in groups])
        data["droplets"] = data["droplets"].loc[droplets,:]
        data["dynamics"] = data["dynamics"][[x in droplets for x in data['dynamics'].id_exp]]

    # Measure empty droplets
    logger.debug('Measure time to reach threshold {} droplets on {} (tmax={})...'.format(parameters["threshold"], parameters["channel"],parameters["max_time"]))
    df = dropsignal.quality.time_to_reach_threshold(data["dynamics"],
                                                    parameters["channel"],
                                                    parameters["threshold"],
                                                    parameters["max_time"])

    # Aggregate the number of empty droplets per groups.
    dr = data['droplets']
    merged = pd.merge(df, data['droplets'].reset_index().loc[:, ['id_exp', 'group']],how='left')
    grouped = merged.groupby('group')
    group_info = pd.DataFrame({'drops':grouped.empty.count(),
                               'average_time':grouped.time_to_reach_threshold.mean(),
                               'empty':grouped.empty.sum().astype(int)})

    # Remove old plots
    for file in glob.glob(os.path.join(parameters["export_path"],"*.png")):
        os.remove(file)

    # Plots !
    logger.debug('Plotting dynamics...')
    plot_dynamics(merged, data['dynamics'],
                  parameters['channel'],
                  parameters['export_path'],
                  parameters['threshold'],
                  parameters['max_time'],
                  not parameters['include_empty'])
    logger.debug('Kernel density estimations...')

    plot_kde(merged if parameters['include_empty'] else merged[merged['empty'] == 0],
             parameters['export_path'])

    # Estimate Poisson parameters
    group_info['lambda'] = -np.log(group_info["empty"]/group_info["drops"])
    group_info['0_class'] = group_info['drops']*np.exp(-group_info['lambda'])
    group_info['1_class'] = group_info['drops']*np.exp(-group_info['lambda'])*group_info['lambda']
    group_info['2plus_class'] = group_info['drops']*(1-(1+group_info['lambda'])*np.exp(-group_info['lambda']))

    # Export
    html = """
    <html charset="UTF-8">
    <head>
    <title>Poisson Dilution</title>
    <style>
    .plot img {{
    max-width:50%
    }}
    </style>
    </head>
    <body>
    <h1>Poisson dilution analysis</h1>
    <p>Droplets are considered empty if the signal in {} stays below {}mV before {}. </p>
    """.format(parameters["channel"],
               parameters["threshold"],
               tick_fmt(parameters["max_time"],None))

    ## Time format.
    group_info['average_time (h:m:s)'] =  group_info['average_time'].apply(seconds_to_hms)
    merged['time_to_reach_threshold (h:m:s)'] =  merged['time_to_reach_threshold'].apply(seconds_to_hms)
    group_info['average_time (minutes)'] =  group_info['average_time'].apply(lambda x:x/60)
    merged['time_to_reach_threshold (minutes)'] =  merged['time_to_reach_threshold'].apply(lambda x:x/60)

    html += '<div class="plot"><img src="KDE.png"/></div>'
    html += group_info.to_html()
    html += '<a href="group_info.csv">Download</a>'
    for g,_ in grouped:
        html += '<div class="plot"><img src="{}.png"/><img src="{}_KDE.png"/></div>'.format(g,g)

    html += '<div style="max-height:300px;overflow:scroll;"> <h3> Raw data </h3><a href="times.csv">Download</a>'
    html += merged.to_html()
    html += '</div></body></html>'

    merged.to_csv(os.path.join(parameters["export_path"], 'times.csv'))
    group_info.to_csv(os.path.join(parameters["export_path"], 'group_info.csv'))

    with open(os.path.join(parameters["export_path"], 'index.html'), 'w') as file:
        file.write(html)

def seconds_to_hms(seconds):
    try:
        minutes = seconds//60
        return "{}:{}:{}".format(int(minutes//60),int(minutes%60),int(seconds%60))
    except ValueError:
        return '-'
def tick_fmt(d, _):
    h = d//60
    return "{}:{}:{}".format(int(h//60),int(h%60),int(d%60))

def plot_dynamics(merged, dynamics, axis, path, threshold, max_time, hide_empty=False):
    formatter = FuncFormatter(tick_fmt)
    if hide_empty:
        merged = merged[merged['empty']==0]
    for group,drops in merged.groupby('group'):
        fig = plt.figure(figsize=(15,10))
        ax = plt.gca()
        ax.xaxis.set_major_formatter(formatter)

        drop_index = frozenset(drops.id_exp)
        color = {0:'C1',1:'C0'}
        is_empty = merged.set_index('id_exp')['empty']
        for idx,v in dynamics[[x in drop_index for x in dynamics.id_exp]].groupby('id_exp'):
            ax.scatter(v["time"],v[axis], color=color[is_empty.loc[idx]],alpha=0.1)
            ax.plot(v["time"],v[axis], color=color[is_empty.loc[idx]])

        ylim = plt.ylim()
        xlim = plt.xlim()

        plt.scatter(drops['time_to_reach_threshold'],
                    [threshold]*len(drops),
                    marker="+",
                    color='r', zorder=100)

        plt.vlines(max_time,*ylim, alpha=.5)
        plt.hlines(threshold,*xlim, alpha=.5)

        plt.ylim(ylim)
        plt.xlim(xlim)

        plt.xlabel('Time (h:m:s)')
        plt.ylabel(axis+' (mv)')
        plt.title(group)
        plt.savefig(os.path.join(path,group+'.png'),bbox_inches='tight')
        plt.close(fig)

def plot_kde(merged, path):
    grid = np.linspace(merged.time_to_reach_threshold.min(), merged.time_to_reach_threshold.max(), 200)
    formatter = FuncFormatter(tick_fmt)
    master_fig = plt.figure(figsize=(15, 10))
    master_ax = plt.gca()

    for group, data in merged.groupby('group'):
        x = data.time_to_reach_threshold.dropna().values
        fig = plt.figure(figsize=(15, 10))
        ax = plt.gca()

        if len(x) > 1 and np.std(x)!=0:
            kde = gaussian_kde(x)
            y = kde.evaluate(grid)

            # Plot on the master_plot
            line = master_ax.plot(grid, y, label=group)
            master_ax.fill_between(grid, y, color=line[0].get_color(), alpha=.1)

            # Plot on the individual plots
            ax.plot(grid, y, color=line[0].get_color())
            ax.fill_between(grid, y, color=line[0].get_color(), alpha=.5)

            ylim = plt.ylim()
            xlim = plt.xlim()
            ax.scatter(data.time_to_reach_threshold, [0]*len(data), marker='|', color='k', alpha=.5)
            plt.ylim(ylim)
            plt.xlim(xlim)
        elif len(x) == 1:
            ax.scatter(data.time_to_reach_threshold, [0]*len(data), marker='|', color='k', alpha=.5)
        ax.xaxis.set_major_formatter(formatter)
        ax.set_xlabel('Time (h:m:s)')
        ax.set_ylabel('Density')
        ax.set_title('Distribution of time to reach the threshold: {}'.format(group))
        plt.savefig(os.path.join(path, group+'_KDE.png'), bbox_inches='tight')
        plt.close(fig)
    plt.sca(master_ax)
    plt.xlabel('Time (h:m:s)')
    plt.ylabel('Density')
    plt.title('Distributions of time to reach the threshold')
    master_ax.xaxis.set_major_formatter(formatter)
    plt.legend()
    plt.savefig(os.path.join(path, 'KDE.png'), bbox_inches='tight')
    plt.close(master_fig)
