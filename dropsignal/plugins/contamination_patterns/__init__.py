"""Contamination patterns

Estimate the distribution of contamination time and positions, and
break-down by categories of nearest neighbors.

Author: Guilhem Doulcier

TODO:
- Refactor those spaghetti.
"""
import os
import glob
import logging

import pandas as pd
import numpy as np
from scipy.stats import gaussian_kde
import matplotlib
matplotlib.use('agg')
from matplotlib.ticker import FuncFormatter
import matplotlib.patches as mpatches
import matplotlib.pyplot as plt

import dropsignal.quality
import dropsignal.plugins.poisson

logger = logging.getLogger('dropsignal.plugins.poisson')

PARAMETERS = {"threshold":{"description":"Value above which the droplet is considered grown","default":.1},
              "channel":{"description":"Channel used to detect empty droplets.", "default":"fluo_1_max"},
              "max_time":{"description":"Limit time to detect non-empty droplets (in minutes).", "default":1*60},
              "empty_groups":{"description":"List of groups containing empty droplets", "default":tuple(('empty','Empty','SMM','LB'))}}


def main(data, protocol, parameters):
    """"""
    # convert to seconds.
    parameters['max_time'] *= 60
    parameters['max_time'] = min(data['dynamics']['time'].max(), parameters['max_time'])

    # Sanity Check
    if parameters["channel"] not in data['dynamics'].columns:
        logger.error('Unknown channel: {}'.format(parameters["channel"]))
        return 1

    # Filter the data.
    droplets = closest(data['droplets'], parameters["empty_groups"])
    droplets.index.name = 'id_exp'
    droplets = droplets[np.logical_not(droplets['inoculated'])].loc[:,['droplet', 'inoculated', 'closest_id', 'closest_group', 'closest_abs_distance', 'closest_distance', 'group']]
    empty_idx = frozenset(droplets.index)
    dynamics = data["dynamics"][[x in empty_idx for x in data["dynamics"].id_exp]]
    if not len(droplets):
        logger.error('No droplet matching: {}'.format(parameters["empty_groups"]))
        return 1

    # Measure empty droplets
    logger.debug('Measure time to reach threshold {} droplets on {} (tmax={}s)...'.format(parameters["threshold"], parameters["channel"], parameters["max_time"]))
    df = dropsignal.quality.time_to_reach_threshold(dynamics,
                                                    parameters["channel"],
                                                    parameters["threshold"],
                                                    parameters["max_time"])
    droplets = pd.merge(df, droplets.reset_index(), how='left')

    # Measure contamination along the train
    droplets['pos'] = pd.cut(droplets.id_exp, 20, precision=1)
    gb = droplets.groupby('pos')
    position = pd.DataFrame({'drops':gb.count()['empty'],
                             'average_time':gb['time_to_reach_threshold'].mean(),
                             'non_contaminated':gb.sum()['empty']})
    position['i'] = [float(x.split(',')[0][1:]) for x in position.index]
    position['f'] = [float(x.split(',')[1][:-1]) for x in position.index]
    position['l'] = position.f-position.i
    plot_position_prop(position, parameters["export_path"])
    plot_position_bar(position, parameters["export_path"])

    # Plot the dynamics
    plot_dynamics(droplets, dynamics,
                  parameters["channel"],
                  parameters["export_path"],
                  parameters["threshold"],
                  parameters["max_time"])

    # Measure the proportion of contamination and time to reach threshold.
    html_prop = ''
    for col in ['group','closest_distance', 'closest_abs_distance', 'closest_group']:
        plot_kde(droplets,parameters["export_path"], col)
        if droplets[col].nunique()>1:
            prop = compute_group_proportions(droplets, col)
            plot_group_prop(prop,parameters["export_path"],col)
            plot_group_time(prop,parameters["export_path"],col)
            html_prop += '<div class="plot"><img src="proportions_{0}.png"/><img src="timetoreach_{0}.png"/></div>'.format(col)


    # Export
    html = """
    <html charset="UTF-8">
    <head>
    <title>Contamination patterns</title>
    <style>
    .plot img {{
    max-width:50%
    }}
    </style>
    </head>
    <body>
    <h1>Contamination patterns</h1>
    <p>Droplets in {} are considered contaminated if the signal in {} go above {}mV before {}. </p>
    """.format(parameters["empty_groups"],
               parameters["channel"],
               parameters["threshold"],
               dropsignal.plugins.poisson.tick_fmt(parameters["max_time"],None))
    html += '<div class="plot"><img src="dynamics.png"/></div>'
    html += '<div class="plot"><img src="KDE_group.png"/>'
    html += '<img src="KDE_closest_group.png"/></div>'
    html += '<div class="plot"><img src="KDE_closest_abs_distance.png"/>'
    html += '<img src="KDE_closest_distance.png"/></div>'
    html += '<div class="plot"><img src="position.png"/><img src="position_time.png"/></div>'
    html += html_prop
    html += droplets.to_html()
    html += '<a href="empty_drops.csv">Download</a>'
    droplets.to_csv(os.path.join(parameters["export_path"], 'empty_drops.csv'))

    with open(os.path.join(parameters["export_path"], 'index.html'), 'w') as file:
        file.write(html)

def plot_kde(droplets, path, groupby):
    grid = np.linspace(droplets.time_to_reach_threshold.min(),
                       droplets.time_to_reach_threshold.max(),
                       200)
    formatter = FuncFormatter(dropsignal.plugins.poisson.tick_fmt)
    master_fig = plt.figure(figsize=(15, 10))
    master_ax = plt.gca()

    for group, data in droplets.groupby(groupby):
        x = data.time_to_reach_threshold.dropna().values
        if len(x) > 1:
            kde = gaussian_kde(x)
            y = kde.evaluate(grid)

            line = master_ax.plot(grid, y, label=group)
            master_ax.fill_between(grid, y, color=line[0].get_color(), alpha=.1)

            master_ax.scatter(data.time_to_reach_threshold, [0]*len(data), marker='|',
                              color=line[0].get_color(), alpha=.5)


    plt.sca(master_ax)
    plt.xlabel('Time (h:m:s)')
    plt.ylabel('Density')
    plt.title('Distributions of time to reach the threshold by {}'.format(groupby))
    master_ax.xaxis.set_major_formatter(formatter)
    plt.legend()
    plt.savefig(os.path.join(path, 'KDE_{}.png'.format(groupby)), bbox_inches='tight')
    plt.close(master_fig)


def plot_dynamics(droplets,dynamics, axis, path, threshold, max_time):
    formatter = FuncFormatter(dropsignal.plugins.poisson.tick_fmt)
    fig = plt.figure(figsize=(15,10))
    ax = plt.gca()
    ax.xaxis.set_major_formatter(formatter)

    color = {0:'C1',1:'C0'}
    is_empty = droplets.set_index('id_exp')['empty']
    for idx,v in dynamics.groupby('id_exp'):
        ax.scatter(v["time"],v[axis], color=color[is_empty.loc[idx]],alpha=0.1)
        ax.plot(v["time"],v[axis], color=color[is_empty.loc[idx]])

    plt.legend(handles=[mpatches.Patch(color='C0', label='Non Contaminated'),
                        mpatches.Patch(color='C1', label='Contaminated')])

    ylim = plt.ylim()
    xlim = plt.xlim()

    plt.scatter(droplets['time_to_reach_threshold'],
                [threshold]*len(droplets),
                marker="+",
                color='r',
                zorder=100)

    plt.vlines(max_time,*ylim, alpha=.5)
    plt.hlines(threshold,*xlim, alpha=.5)

    plt.ylim(ylim)
    plt.xlim(xlim)

    plt.xlabel('Time (h:m:s)')
    plt.ylabel(axis+' (mv)')
    plt.title('Dynamics of empty drops')
    plt.savefig(os.path.join(path,'dynamics.png'),bbox_inches='tight')
    plt.close(fig)


def closest(droplets, empty_groups):
    '''Add the 'closest_inoculated' column to the droplets dataframe'''
    if type(empty_groups) is str:
        droplets['inoculated'] = droplets['group'] == empty_groups
    else:
        empty_groups = frozenset(empty_groups)
        droplets['inoculated'] = [x not in empty_groups for x in droplets['group']]

    droplets = dropsignal.quality.closest(droplets, 'inoculated', 'closest_id')
    droplets['closest_group'] = droplets.ix[droplets['closest_id'].values,'group'].values
    droplets['closest_distance'] = droplets['closest_id'] - droplets.index
    droplets['closest_abs_distance'] = np.abs(droplets['closest_distance'])

    return droplets


def plot_position_prop(position,path):

    fig = plt.figure(figsize=(20,7))
    plt.bar(left=position['i'], height=[1]*len(position),
            width=position['l'], color='C0', align='edge',alpha=.66)
    plt.bar(left=position['i'],
            height=(position['drops']-position['non_contaminated'])/position['drops'],
            width=position['l'], color='C1', align='edge')
    plt.legend(handles=[mpatches.Patch(color='C0', label='Non Contaminated'),
                        mpatches.Patch(color='C1', label='Contaminated')])

    plt.xticks(position['i'])
    for _,row in position.iterrows():
        plt.text(row.i+row.l/2, (row.drops-row.non_contaminated)/row.drops,
                 '{}/{}'.format(int(row.drops-row.non_contaminated), int(row.drops)),
                 horizontalalignment='center')

    plt.yticks([0,.2,.4,.6,.8,1],[0,'20%','40%','60%',"80%","100%"])
    plt.ylim(0,1)
    plt.xlim(position['i'].min(),position['f'].max())
    plt.title('Contaminations along the train')
    plt.xlabel('Position')
    plt.savefig(os.path.join(path,'position.png'),bbox_inches='tight')
    plt.close(fig)

def plot_position_bar(position,path):
    fig = plt.figure(figsize=(20,7))
    formatter = FuncFormatter(dropsignal.plugins.poisson.tick_fmt)
    plt.bar(left=position['i'], height=position['average_time'],
            width=position['l'], color='C0', align='edge')
    plt.xticks(position['i'])
    for _,row in position.iterrows():
        plt.text(row.i+row.l/2, row.average_time,
                 dropsignal.plugins.poisson.tick_fmt(row.average_time,None),
                 horizontalalignment='center')

    plt.gca().yaxis.set_major_formatter(formatter)
    plt.xlim(position['i'].min(),position['f'].max())
    plt.title('Average time to contamination along the train')
    plt.xlabel('Position')
    plt.savefig(os.path.join(path,'position_time.png'),bbox_inches='tight')
    plt.close(fig)

def plot_group_prop(groupdf,path,title):
    fig=plt.figure(figsize=(12,5))
    plt.bar(left=range(len(groupdf)),height=[1]*len(groupdf),
            color='C0',
            align='edge',
            alpha=.66)
    plt.bar(left=range(len(groupdf)),
            height=groupdf.contaminated_prop,
            color='C1',
            align='edge')

    for n,(k,row) in enumerate(groupdf.iterrows()):
        plt.text(n+0.4,
                 row.contaminated_prop,
                 '{}/{}'.format(int(row.drops-row.non_contaminated),int(row.drops)),
                 horizontalalignment='center')
    plt.legend(handles=[mpatches.Patch(color='C0', label='Non Contaminated'),
                        mpatches.Patch(color='C1', label='Contaminated')])

    plt.yticks([0,.2,.4,.6,.8,1],[0,'20%','40%','60%',"80%","100%"])
    plt.title('Contaminations proportions by {}'.format(title))
    plt.ylim(0,1.05)
    plt.xticks(np.arange(len(groupdf))+0.4,groupdf.index)
    plt.savefig(os.path.join(path,'proportions_{}.png'.format(title)),bbox_inches='tight')
    plt.close(fig)

def plot_group_time(groupdf,path,title):
    fig=plt.figure(figsize=(12, 5))
    formatter = FuncFormatter(dropsignal.plugins.poisson.tick_fmt)
    plt.bar(left=range(len(groupdf)), height=groupdf['average_time'],
             color='C0', align='edge')

    for n, (k, row) in enumerate(groupdf.iterrows()):
        plt.text(n+0.4, row.average_time,
                 dropsignal.plugins.poisson.tick_fmt(row.average_time,None),
                 horizontalalignment='center')

    plt.gca().yaxis.set_major_formatter(formatter)
    plt.title('Average time to reach the threshold by {}'.format(title))
    plt.xticks(np.arange(len(groupdf))+0.4, groupdf.index)
    plt.savefig(os.path.join(path, 'timetoreach_{}.png'.format(title)), bbox_inches='tight')
    plt.close(fig)


def compute_group_proportions(droplets, by):
    gb = droplets.groupby(by)
    prop = pd.DataFrame({'drops':gb.count()['empty'],
                         'average_time':gb['time_to_reach_threshold'].mean().fillna(0),
                         'non_contaminated':gb['empty'].sum()})
    prop['contaminated'] = prop.drops - prop.non_contaminated
    prop['non_contaminated_prop'] = prop.non_contaminated/prop.drops
    prop['contaminated_prop'] = 1-prop['non_contaminated_prop']
    return prop
