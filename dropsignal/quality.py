""" quality.py -- Quality check """

from itertools import chain
from functools import partial
import logging
import numpy as np
import pandas as pd
logger = logging.getLogger('dropsignal.quality')


def qc_length(runs, threshold=3, **_):
    """Mark the runs that are too long or too short in time"""
    length_issue = (runs.duration-runs.duration.mean())/runs.duration.std()
    runs[length_issue > threshold].tags.apply(lambda x: x.append('too_long'))
    runs[length_issue < -threshold].tags.apply(lambda x: x.append('too_short'))
    return runs

def qc_peak_nb(runs, protocol, **_):
    """Marks the run that have too few or too many droplets"""

    expected = protocol["expected_droplets"]
    runs[runs.peaks > expected].tags.apply(lambda x: x.append('too_much_droplets'))
    runs[runs.peaks < expected].tags.apply(lambda x: x.append('too_few_droplets'))
    runs[runs.peaks < .90 * expected].tags.apply(lambda x: x.append('partial_train'))

    if runs.shape[0] > 3:
        quirk = list(chain([False],
                           np.logical_and((runs.peaks[:-2].values == runs.peaks[2:].values),
                                          (runs.peaks[:-2].values != runs.peaks[1:-1].values)),
                           [False]))
        runs[quirk].tags.apply(lambda x: x.append('probable_misscount'))
    return runs

def qc_coalescence(runs, thres=1, **_):
    """ Marks runs as coalescent """
    diff = runs.peaks.diff(1)

    def add_tag(taglist, tag, ifnot='probable_misscount'):
        """ Add tag to the list if another tag is not present """
        if ifnot not in taglist:
            taglist.append(tag)

    after_misscount = ["probable_misscount" in x for x in runs.tags]
    during_misscount = [False]+["probable_misscount" in x for x in runs.tags[:-1]]
    misscount = np.logical_or(after_misscount, during_misscount)

    # We define coalescence events when a droplet is lost.
    coal_time = np.logical_and(np.logical_and(diff >= -thres, diff < 0), np.logical_not(misscount))
    runs[coal_time].tags.apply(partial(add_tag, tag="coalescence_event"))

    # We define dramatic coalescence events when several droplets are lost.
    dcoal_time = np.logical_and(np.logical_and(diff < -thres, diff < 0), np.logical_not(misscount))
    runs[dcoal_time].tags.apply(partial(add_tag, tag="dramatic_coalescence"))

    # the following runs are tagged as coalesced.
    first = min(coal_time.argmax(), dcoal_time.argmax())
    if first:
        runs[runs.index > first].tags.apply(partial(add_tag, tag="coalesced", ifnot=''))


def annotate_droplets(droplet_dyn):
    """ Droplet annotation"""
    values = [x for x in droplet_dyn.columns
              if '_' in x and x.split("_")[-1] in frozenset(('max','value','mean','median'))]
    values += [x for x in ('speed','size') if x in droplet_dyn.columns]
    droplets = droplet_dyn.groupby('id_exp').apply(max).loc[:, ['run'] + values]
    droplets.columns = ['max_run'] + ['max_'+x for x in values]
    droplets['records'] = droplet_dyn.groupby('id_exp').count().run
    droplets['tags'] = [list() for x in droplets.index]
    droplets['index'] = droplets.index
    droplets['creation_time'] = droplet_dyn[droplet_dyn.run==0].absolute_time

    # Coalesced droplets.
    N = droplets.max_run.max()
    droplets.loc[droplets.max_run != N,'tags'].apply(lambda x: x.append('coalesced'))
    return droplets

def closest(droplets, column_inoculated='expected_growth', column_closest='closest_inoculated'):
    '''Add the 'column_closest' column to the droplets dataframe'''
    if 'index' not in droplets.columns:
        if 'id_exp' in droplets.columns:
            droplets['index'] = droplets.id_exp
        else:
            droplets['index'] = droplets.index
    droplets[column_closest] = droplets['index'].copy()
    droplets.set_index('index', inplace=True)

    # Back...
    j = np.min(droplets[droplets[column_inoculated]].index.values)
    for i in droplets.index.values:
        if droplets.ix[i, column_inoculated]:
            j = i
        else:
            droplets.ix[i, column_closest] = j

    # and forth...
    j = np.max(droplets[droplets[column_inoculated]].index.values)
    for i in droplets.index.values[::-1]:
        if droplets.ix[i, column_inoculated]:
            j = i
        else:
            if abs(droplets.ix[i, column_closest] - i) > abs(j - i):
                droplets.ix[i, column_closest] = j
    return droplets

def detect_contamination(droplets, droplet_dyn, channel_order, limit_time, threshold):
    '''Add the 'grown' column to the droplets dataframe containing the first
       time at which the growth `channel` value is superior than `threshold`.

       Add the `contamination` tag to the droplet which were not
    supposed to grow (expected_growth==0) but were detected as grown before `limit_time`.
    '''
    droplets["grown"] = np.nan

    # Channel selection
    channel = ''
    i = 0
    while channel not in droplet_dyn.columns and i < len(channel_order):
        channel = channel_order[i]+'_max'
        i += 1

    if threshold == 0 or channel not in droplet_dyn.columns:
        logger.info('Contamination detection ignored (Time: {}, threshold: {}, Channel:{})'.format(limit_time, threshold, channel))
        return droplets, channel
    else:
        logger.info('Using channel {} to detect contamination (threshold: {} before {})'.format(channel, threshold, limit_time ))

    # Test for growth
    for drop, timeseries in droplet_dyn.groupby("id_exp"):
        #if float(timeseries.query('time>{}'.format(time)).sort_values('time').head(1)[channel]) > threshold:
        #    droplets.ix[drop, 'grown'] = 1
        try:
            droplets.ix[drop, 'grown'] = next((row['time']
                                               for i,row
                                               in timeseries.sort_values('time').iterrows()
                                               if row[channel]>threshold))
        except StopIteration:
            droplets.ix[drop, 'grown'] = 0

    # Update tags
    contaminations = np.logical_and(droplets.expected_growth == 0, droplets.grown < limit_time)
    if contaminations.sum():
        droplets.loc[contaminations, 'tags'].apply(lambda x: x.append('contamination'))
        logger.warning("{} droplets tagged as contaminated".format(contaminations.sum()))
    return droplets.reset_index(), channel

def time_to_reach_threshold(dynamics, channel, threshold, time_max):
    droplets = []
    for drop, timeseries in dynamics.groupby("id_exp"):
        # Find the first measure abobe the threshold.
        ts = timeseries.sort_values('time').reset_index()
        try:
            idx = next((i
                         for i, x
                         in enumerate(ts[channel])
                         if x > threshold))
        except StopIteration:
            idx = -1

        # Linear interpolation.
        if idx > 0:
            time_i = ts.loc[idx-1,'time']
            delta_t = ts.loc[idx,'time'] - time_i
            v_i = ts.loc[idx-1,channel]
            delta_v = ts.loc[idx,channel] - v_i
            time = ((threshold-v_i) * (delta_t/delta_v)) + time_i
        elif idx == 0:
            time = 0
        else:
            time = np.nan
        droplets.append({"id_exp":drop,
                         "empty":time > time_max or np.isnan(time),
                         "time_to_reach_threshold":time})
    return pd.DataFrame(droplets)
