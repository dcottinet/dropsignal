""" Rebuild -- rebuild timeseries"""
import logging

from itertools import combinations

import numpy as np
import pandas as pd

logger = logging.getLogger('dropsignal')

def concatenate_channels(peaks_run, columns=('mean', 'max','shift', 'std','cov','median','area')):
    """Concatenate a dictionnary of channels
           Args:
               peaks_runs (dict): Contains a dataframe by channel.
               columns (iterable): Columns specific to the channel.
           Returns: A pd.Dataframe with a <channel>_<col> column per <channel> and <columns>
               and using the others columns as index.
    """
    dyn = []
    if len(peaks_run)>1:
        for channel, peak in peaks_run.items():
            if channel != '__measure__':
                dyn.append(peak.rename(columns=dict([(col, channel+"_"+col) for col in columns if col in peak.columns])))
                idx = [x for x in dyn[-1].columns if "_".join(x.split("_")[:-1]) != channel]
                dyn[-1] = dyn[-1].set_index(idx)
        df = pd.concat(dyn, 1)
        df.reset_index(inplace=True)
        # Add the columns "time", "speed", "size" from the __measure__ channel.
        if '__measure__' in peaks_run.keys():
            df = pd.merge(df, peaks_run['__measure__'], left_on='id_run', right_on='id_run')

        if "Time_value" in df.columns:
            df.rename(columns={"Time_value":"time"}, inplace=True)
        return df
    else:
        # Run 0 does not have the measures.
        return peaks_run['__measure__']

def droplet_dynamics(runs, peaks, coalescence_events=None):
    """ Concatenate the runs in a single data frame

    Args:
        runs (iterable): Id of the runs to use
        peaks (iterable of dict): Each dictionnary correspond to a run,
            each one contains a dataframe by channel with columns: max, mean, value,
            center, id_run, time.
        coalescence_event (dict): Mapping run->coalesced droplet (can be several).
    Returns:
        A single dataframe with:
           - center, id_run, time and run from the runs,
           - CHANNEL_max, CHANNEL_mean, CHANNEL_value from each channel
           - id_exp and offset according to the coalescence events.
    """
    droplet_dyn = []
    for run in runs:
        # We start by concatenating all channels.
        dyn = concatenate_channels(peaks[run])
        dyn['run'] = run

        # Then we add an indexing according to the coalescence events.
        dyn['id_exp'] = np.nan
        dyn['offset'] = 0
        if coalescence_events is not None and len(coalescence_events):
            for k, id_drop in coalescence_events.items():
                if run >= k:
                    try:
                        for i in id_drop:
                            dyn['offset'] += np.int_(dyn.id_run.values >= i)
                    except TypeError:
                        dyn['offset'] += np.int_(dyn.id_run.values >= id_drop)
        dyn['id_exp'] = np.int_(dyn.id_run) + np.int_(dyn.offset)
        droplet_dyn.append(dyn)

    droplet_dyn = pd.concat(droplet_dyn).reset_index(drop=True)

    logger.info('Computing time relative to droplet creation')
    droplet_dyn['relative_time'] = np.nan
    try:
        for _, data in droplet_dyn.groupby('id_exp'):
            relative_times = data['time'] -  data[data.run==0].time.values[0]
            droplet_dyn.loc[relative_times.index,'relative_time'] = relative_times.values
    except Exception:
        logger.error('Failed to compute times relative to droplet creation')
    return droplet_dyn

def find_coalescent_position(previous, current):
    """Find the droplet that increased the more in size."""
    column = 'size'
    if '__measure__' not in previous.keys() or '__measure__' not in current.keys():
        logger.warning('Photodiode data missing. Impossible to find the position of the coalesced droplet.')
        return None
    else:
        if 'size' not in previous['__measure__'].columns or 'size' not in current['__measure__'].columns:
            logger.warning('Droplet size data missing. Trying to use the number of measured points.')
            column = 'measure_points'
            if column not in previous['__measure__'].columns or column not in current['__measure__'].columns:
                logger.warning('Number of measured points missing. Impossible to find the position of the coalesced droplet.')
                return None

        # Align the two runs as much as possible.
        merged = pd.merge(previous['__measure__'].loc[:,['id_run', column]],
                          current["__measure__"].loc[:,['id_run', column]],
                          how='left', left_on='id_run', right_on='id_run',
                          suffixes=['_prev', '_curr'])

        # Count the number of coalescence events
        current_size = merged[column+'_curr'].dropna().values
        number_coal = merged.shape[0] - len(current_size)

        # Compute all the difference in size for different offset.
        diff = {d:np.abs(current_size-merged[column+'_prev'].values[d:-number_coal+d if -number_coal+d<0 else None])
                for d in np.arange(number_coal+1)}

        # Compute all the positions possible for the coalescence events
        keys = list(combinations(np.arange(merged.shape[0]),number_coal))

        # Compute all the total difference in size for all different coalescence events positions.
        delta = np.zeros(len(keys))
        for i,k in enumerate(keys):
            for delay, (start,end) in enumerate(zip([0]+list(k),list(k)+[None])):
                delta[i] += diff[delay][start:end].sum()

        # Get back the position of the drop.
        pos = [merged.ix[i,'id_run'] for i in keys[np.argmin(delta)]]
        reason = 'Minimal droplet size total variation: {} | position of breakpoints: {}'.format(np.min(delta), keys[np.argmin(delta)])
        return pos, reason

def coalescence_detection(coalescent_runs, peaks):
    coalescence_events = {}
    for run in coalescent_runs:
        try:
            candidate,reason = find_coalescent_position(peaks[run-1],peaks[run])
        except Exception as ex:
            logger.exception("Error in coalescence detection  between run {} and {}".format(run-1, run))
            candidate = None
        if candidate != None:
            coalescence_events[run] = candidate
            logger.info('Between run {} and {} best candidate is drop {} ({}).'.format(run-1,
                                                                                       run,
                                                                                       coalescence_events[run],
                                                                                       reason))
    return coalescence_events
