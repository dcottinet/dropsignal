""" Models.py -- models to fit """
import logging
import warnings

import pandas as pd
import numpy as np
import scipy.optimize

logger = logging.getLogger('dropsignal.models')

def fit_logistics(droplet_dyn, column):
    models = []
    def logistic(t, v0, r, Nmax, lag):
        return v0+Nmax/(1+np.exp((4*r/Nmax)*(lag-t)+2))
    runtime = []
    optwarning = []

    for drop,df in droplet_dyn.groupby('id_exp'):
        with warnings.catch_warnings():
            warnings.simplefilter("error", scipy.optimize.OptimizeWarning)
            t0 = df.time.values[0]
            v0 = df[column].min()
            dv = df[column].max()-df[column].min()
            dt = df.time.values[-1]-df.time.values[0]
            if dv>0:
                r0 = np.log(dv) / np.log(dt)
            else:
                r0 = 0
            try:
                popt, pcov = scipy.optimize.curve_fit(logistic,
                                                      df.time.values-t0,
                                                      df[column].values,
                                                      p0=(v0, r0, df[column].max(), 0))
            except RuntimeError:
                runtime.append(drop)
            except scipy.optimize.OptimizeWarning:
                optwarning.append(drop)
            except TypeError:
                if df.shape[0] < 3:
                    logger.error("Not enough points in dynamics nb {}".format(drop))
            except ValueError:
                logger.error("Value error in dynamics nb {}".format(drop))
            else:
                models.append({'id_exp':drop,
                               'growth_rate':popt[1],
                               'carrying_capacity':popt[2],
                               'lag':popt[3],
                               't0':t0,
                               'v0':popt[0],
                               'tf':df.time.values[-1]-df.time.values[0]})
    if len(runtime):
        logger.warning("{} fitting failed (ran out of time): {}".format(len(runtime), runtime))
    if len(optwarning):
        logger.warning(("{} fitting failed "
                         "(covariance of the parameters can not be estimated): "
                         "{}").format(len(optwarning),
                                      optwarning[:min(10, len(optwarning)-1)]))

    models = pd.DataFrame(models)
    return models
