// selector.js - Droplet selector object.
// This file is part of the dropsignal package.
// Copyright 2018 Guilhem Doulcier, Licence GNU AGPL3+
// ---

function DropletSelector(droplets, div, plate){
    this.div = div
    this.droplets = droplets
    this.plate = plate

    this.segment_mode = false

    console.log('Selector initialized: '+droplets.length+' droplets')

    // Init the selection
    this.droplets.forEach(function(d){d.selected=false})

    // Set the UI
    var self = this
    this.a_download = this.div.append('a')
	.text('Download')
	.attr('class','btn btn-primary')
	.attr('target','_blank')
	.attr('download','sorting.dat')
    this.div.append('button')
	.text('Clear')
	.on('click',function(){self.clear()})
	.attr('class','btn btn-default')
    var form = this.div.append('form')
	.attr('class','form-group')
	.style('display','inline')
    form.append('label').text('Segment mode')
    form.append('input')
	.attr('type','checkbox')
	.on('change', function(){self.segment_mode = !self.segment_mode; self.update()})
    this.div_status = this.div.append('div')
    this.div_list = this.div.append('pre')
    this.update()
}


DropletSelector.prototype.clear = function(){
    // Clear the selection
    this.droplets.forEach(function(d){d.selected=false})
    this.update()
}


// The following methods toggle the selection on a drop, well, column or row.

DropletSelector.prototype.select_droplet = function(id_exp, force){
    if (id_exp instanceof Array){
	this.droplets.filter(function(d){return id_exp.indexOf(d.id_exp)>=0}).forEach(
	    function(elt){
		elt.selected = force!=undefined?force:!elt.selected})
    }
    else{
	var elt = this.droplets.filter(function(d){return d.id_exp==id_exp})[0]
	elt.selected = force!=undefined?force:!elt.selected
    }
    this.update()
}

DropletSelector.prototype.select_well = function(well){
    var selection = this.droplets.filter(function(d){return d.well==well})
    var selected = d3.sum(selection, function(d) { return d.selected })
    var new_value = selected != selection.length
    selection.forEach(function(d){d.selected=new_value})
    this.update()
}

DropletSelector.prototype.select_column = function(well){
    var selection = this.droplets.filter(function(d){return d.well.substring(1)==well})
    var selected = d3.sum(selection, function(d) { return d.selected })
    var new_value = selected != selection.length
    selection.forEach(function(d){d.selected=new_value})
    this.update()
}

DropletSelector.prototype.select_row = function(well){
    var selection = this.droplets.filter(function(d){return d.well[0]==well})
    var selected = d3.sum(selection, function(d) { return d.selected })
    var new_value = selected != selection.length
    selection.forEach(function(d){d.selected=new_value})
    this.update()
}

DropletSelector.prototype.update = function(){
    // Update the selector and its UI.
    this.segments = build_segments(this.droplets, this.segment_mode)

    // Check the segment list.
    var check = check_segments(this.segments)

    this.div_status.html('<strong>'+(check[0]?'Valid sorting':'Sorting might be invalid:')+' </strong>'+check[1])
	.attr('class', 'alert'+(check[0]?' alert-success':' alert-warning'))
    var exported = export_sorting_list(this.segments)
    this.a_download.attr('href', 'data:attachment/text,' + encodeURI(exported))
    this.div_list.text(exported)
    this.plate.draw(this.droplets)
}

function build_segments(droplets, segment_mode){
    // Build the segment list.
    console.log('Build the segment list, segment mode is ', segment_mode)
    var seg = []
    var current = 'start'
    var len = 0

    // Loop through droplets
    droplets.forEach(function(d){
	if ((d.selected != current && segment_mode == true)||((d.selected == true||current=='start'||current==true) && segment_mode==false)){
	    if (seg.length){
		seg[seg.length-1].len = len
	    }
	    seg.push({type:d.selected==true?'selected':'waste', start:d.id_exp})
	    len = 0
	}
	len++
	current = d.selected
    })

    // If nothing was selected, make one segment with full length.
    // Esle, add the length of the last segment.
    if (seg.length == 0){
	seg = [ {type:'waste', len: droplets.length}]}
    else{
	seg[seg.length-1].len = len
    }

    return seg
}

function check_segments (seg){
    // Check wether the segments form a valid sorting.
    var s = 0
    var w = 0
    var sum_w_len = 0
    var sum_w_fill = 0
    var mx_s = 0
    seg.forEach(function(d){
	if (d.type == 'selected'){
	    s++
	    mx_s = Math.max(mx_s,d.len)
	}
	else{
	    w++
	    sum_w_len += d.len
	    sum_w_fill += Math.floor((d.len-1)/50)
	}
    })
    if (mx_s > 50){
	return [false,'Selection segment too long: '+mx_s]
    }
    if (s+w+sum_w_fill>95){
	return [false, 's + w + sum((W_i-1)/50) > 95']}
    if (s+sum_w_len<95){
	return [false, 's + sum(W_i) < 95']}
    return [true, s+' groups']
}

function export_sorting_list(segments){
    // Export the selection as a list for the sorter.
    var txt = ''
    var offset = 1
    console.log('Exporting segment list', segments, "with offset ", offset)
    segments.forEach(function(d) {
	if (d.type=='selected'){
	    txt= txt +(d.start+offset)+(d.len!=1?' '+(d.start+offset+d.len-1):'')+'\r\n'
	}
    })
    return txt
}
