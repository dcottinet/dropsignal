// scatter.js - Display the dynamics as a scatter plot.
// This file is part of the dropsignal package.
// Copyright 2018 Guilhem Doulcier, Licence GNU AGPL3+
// ---

function timeFormat(num) {
    var h = Math.floor( num / 3600 )
    var m = Math.floor((num - h * 3600) / 60 )
    var s = num - (h * 3600 + m * 60)
    return ( h < 10 ? '0' + h : h ) + ':' + ( m < 10 ? '0' + m : m ) + ':' + ( s < 10 ? '0' + s : s )
}

var timeseries
var getParameterByName, setParameterByName // Defined in uri_variables.js

function Scatter(droplets, dynamics, svg, ui, selector){
    this.svg = svg
    this.ui = ui
    this.rect = svg.node().getBoundingClientRect()
    this.selector = selector
    ui.attr('class','navbar navbar-default').style('margin-top','-20px')

    // Get variable form URL
    var param_xvar = getParameterByName('xvar')
    var param_yvar = getParameterByName('yvar')
    var param_xrun = getParameterByName('xrun')
    var param_yrun = getParameterByName('yrun')

    // Selector
    var form = ui.append('form').attr('class','navbar-form')
    this.xvar = param_xvar ? param_xvar : 'id_exp'
    this.yvar = param_yvar ? param_yvar : undefined

    // Mouseover text
    this.tooltip = ui.append('div').style('text-align','center').attr('class','tooltip_peaks')
    var overtext = this.tooltip.append('div')
    this.tooltip.append('span').text('(Click to apply action)').style('color','gray').style('font-family','arial').style('font-size','0.6em')

    // Data
    this.droplets = droplets
    this.droplets = distance(droplets)
    this.dynamics = d3.nest()
	.key(function(d) { return d.id_exp })
	.key(function(d) { return d.run })
	.entries(dynamics)
    this.runs = d3.map(dynamics, function(d){return d.run}).keys()

    // Define scales
    this.yScales = {}
    this.xScales = {}
    this.min = {}
    this.max = {}
    this.keys = []
    this.keys_norun = {}

    // Prepare scales and extract keys for droplet specific values.
    for (var key in droplets[0]) {
	if (droplets[0].hasOwnProperty(key)) {
	    if (key != 'tags'){
		this.keys.push(key)
		this.keys_norun[key] = 1
		this.min[key] = d3.min(droplets, function(d) {return d[key]})
		this.max[key] = d3.max(droplets, function(d) {return d[key]})
		if (key != 'well' && key != 'group'){
		    this.yScales[key] = d3.scaleLinear().range([this.rect.height-30,30]).domain([this.min[key],this.max[key]])
		    this.xScales[key] = d3.scaleLinear().range([30,this.rect.width-30]).domain([this.min[key],this.max[key]])
		} else {
		    this.yScales[key] = d3.scaleBand()
			.domain(d3.map(this.droplets, function(d){return d[key]}).keys())
			.range([this.rect.height-30,30])
		    this.xScales[key] = d3.scaleBand()
			.domain(d3.map(this.droplets, function(d){return d[key]}).keys())
			.range([30,this.rect.width-30])
		}
	    }
	}
    }

    // Prepare scale and extract keys for dynamics.
    for (var key in this.dynamics[0].values[0].values[0]){
	if (this.dynamics[0].values[0].values[0]) {
	    if (key != 'id_exp' && key != 'run'){
		this.keys.push(key)
		this.yvar = (this.yvar == undefined && key.indexOf('mean') !== -1) ? key : this.yvar
		this.min[key] = d3.min(dynamics, function(d) {return d[key]})
		this.max[key] = d3.max(dynamics, function(d) {return d[key]})
		this.yScales[key] = d3.scaleLinear().range([this.rect.height-30,30]).domain([this.min[key],this.max[key]])
		this.xScales[key] = d3.scaleLinear().range([30,this.rect.width-30]).domain([this.min[key],this.max[key]])
		console.log('key', key, this.min[key], this.max[key])
	    }
	}
    }



    // Draw the points ///
    var self = this
    var points = svg.append('g').attr('id','points')
    points.selectAll('circle')
	.data(droplets)
	.enter()
	.append('circle')
	.attr('r', 3)
	.on('mouseover', function(d){
	    var span_open = ' <span class="label" style="margin-right:.1em;background-color:'+timeseries.groupColor[d.group]+'"> '
	    var span_close = ' </span> '
	    var value = get_value(d, self.dynamics, self.yvar, self.yrun)
	    self.tooltip.style('display', 'block')
	    var fmt = d3.format('.5g')
	    overtext.html(span_open + 'Droplet: ' + d.id_exp + span_close
			  + span_open + 'Well: ' + d.well + span_close
			  +'<table  style="width:100%" class="table-condensed table-bordered">'
			  +'<th>Value</th><td> '+fmt(value)+'</td></tr>'
			  + span_open + d.group + span_close + d.tags)})
	.on('mouseout', function() {
	    self.tooltip.style('display', 'none')	    })
	.on('click', function(d) {
	    d3.event.stopImmediatePropagation()
	    if (self.mode=='signal'){
		var target = 'signal.html?drop='+d.id_exp+'&run='+self.yrun+'&channel='+self.yvar
		if (d3.event.ctrlKey){
		    window.open(target)}
		else{
		    window.location.assign(target, '_blank')
		}
	    } else if (self.mode=='sorting'){
		self.selector.select_droplet(d.id_exp)
		self.draw()
	    }
	   })
	.on('mousemove', function() {
	    self.tooltip.style('top', (d3.event.layerY + 10) + 'px')
		.style('left', (d3.event.layerX + 10) + 'px')
	    })
	.attr('fill',function(d){return timeseries.groupColor[d.group]})

    this.xaxis = svg.append('g').attr('id','xaxis')
    this.yaxis = svg.append('g').attr('id','yaxis')

    /// Scale selector ////
    var xvar = this.xvar
    var yvar = this.yvar

    form.append('label').text('X: ')
    var abs = form.append('select')
    form.append('label').text('Run: ')
    var abs_run = form.append('select')
    form.append('label').text('Y: ').style('margin-left','10px')
    var ord = form.append('select')
    form.append('label').text('Run: ')
    var ord_run = form.append('select')
    var ord_run_slider = form.append('input')

    abs_run.property('disabled', +(this.xvar in this.keys_norun))
    ord_run.property('disabled', +(this.yvar in this.keys_norun))
    ord_run_slider.property('disabled', +(this.yvar in this.keys_norun))

    // Drag and drop selection
    function select(x0,x1,y0,y1) {
	var xmin = Math.min(self.xScales[self.xvar].invert(x0),self.xScales[self.xvar].invert(x1)),
	    xmax = Math.max(self.xScales[self.xvar].invert(x0),self.xScales[self.xvar].invert(x1)),
	    ymin = Math.min(self.yScales[self.yvar].invert(y0),self.yScales[self.yvar].invert(y1)),
	    ymax = Math.max(self.yScales[self.yvar].invert(y0),self.yScales[self.yvar].invert(y1))
	var selection = self.droplets.filter(function(d){
	    var x = get_value(d, self.dynamics, self.xvar, self.xrun)
	    var y = get_value(d, self.dynamics, self.yvar, self.yrun)
	    return (x > xmin) && (x < xmax) && (y > ymin) && (y < ymax)})
	self.selector.select_droplet(selection.map(function(d){return d.id_exp}),1)
	self.draw()
    }
    var selecting = false
    svg.on('mousedown', function(){
	var p = d3.mouse(this)
	selecting = true
	svg.append('rect').attr('class', 'selection')
	    .style('fill', 'gray')
	    .style('opacity', .3)
	    .style('stroke', 'gray')
	    .attr('x0', p[0]).attr('y0', p[1])
	    .attr('x', p[0]).attr('y', p[1])
    })
    svg.on( 'mousemove', function() {
	if (selecting){
	    var s = svg.select('rect.selection')
	    if( !s.empty()){
		var p = d3.mouse(this)
		s.attr('x', Math.min(s.attr('x0'), p[0]))
		s.attr('y', Math.min(s.attr('y0'), p[1]))
		s.attr('width', Math.abs(p[0] - s.attr('x0')))
		s.attr('height', Math.abs(p[1] - s.attr('y0')))
	    }
	}
    })
    d3.select('body').on('mouseup', function(){
	selecting = false
	svg.select('rect.selection').remove()
    })

    svg.on('mouseup', function(){
	if (selecting){
	    selecting = false
	    var s = svg.select('rect.selection')
	    if(!s.empty()){
		var p = d3.mouse(this)
		select(p[0],s.attr('x0'),p[1],s.attr('y0'))
		console.log('will remove s', s)
	    }
	    s.remove()
	}
    })

    // Channel selector for the x-axis.
    abs.on('change',function(){self.xvar = this.options[this.selectedIndex].value
				abs_run.property('disabled', self.xvar in self.keys_norun)
				self.draw(0)})
    abs.selectAll('option').data(this.keys).enter().append('option')
	.attr('value',function(d){return d}).text(function(d){return d})
	.property('selected', function(d){return d==xvar})

    // Run selector for the x-axis.
    abs_run.on('change',function(){self.xrun = this.options[this.selectedIndex].value
				   self.draw(500)})
    abs_run.selectAll('option').data(this.runs).enter().append('option')
	.attr('value',function(d){return d}).text(function(d){return d})
	.property('selected', function(d,i){return param_xrun?d==param_xrun:i==0})

    // Channel selector for the y-axis.
    ord.on('change',function(){self.yvar = this.options[this.selectedIndex].value
			       ord_run.property('disabled', self.yvar in self.keys_norun)
			       ord_run_slider.property('disabled', self.yvar in self.keys_norun)
			       self.draw(0)})
    ord.selectAll('option').data(this.keys).enter().append('option')
	.attr('value',function(d){return d}).text(function(d){return d})
	.property('selected', function(d){return d==yvar})

    // Run selector for the y-axis.
    ord_run.on('change',function(){self.yrun = this.options[this.selectedIndex].value
				   ord_run_slider.node().value = this.selectedIndex
				   self.draw(500)})
    ord_run.selectAll('option').data(this.runs).enter().append('option')
	.attr('value',function(d){return d}).text(function(d){return d})
	.property('selected', function(d,i){return param_yrun?d==param_yrun:i==0})

    ord_run_slider
	.attr('type', 'range').attr('min',0).attr('step',1)
	.style('display','inline').style('width','30%')
	.attr('max', ord_run.node().options.length-1)
	.attr('value', ord_run.node().selectedIndex)
	.on('change', function(){
	    ord_run.node().selectedIndex = this.value
	    self.yrun = ord_run.node().options[this.value].value
	    self.draw(500)
	})

    /// Mode selector ///
    this.mode = 'sorting'
    var self = this
    var mode = form.append('div').style('float','right')
    mode.append('strong').text('Action: ')
    mode.append('label').text('Signal')
	.attr('data-placement','left')
	.attr('data-toggle','tooltip')
	.attr('title','Click to go to the raw signal. CTRL+click open in a new window.')
    mode.append('input').attr('type','radio').attr('name','mode').on('change',function(){self.mode = 'signal'})
    mode.append('label').text('Sorting')
	.attr('data-placement','left')
	.attr('data-toggle','tooltip')
	.attr('title','Click to select/unselect for sorting')
    mode.append('input').attr('type','radio').attr('name','mode').property('checked',true).on('change',function(){self.mode = 'sorting'})

    // Set the default run.
    this.yrun = ord_run.node().options[ord_run.node().selectedIndex].value
    this.xrun = abs_run.node().options[abs_run.node().selectedIndex].value

    // Draw everything ///
    this.draw(0)
}

Scatter.prototype.draw = function (duration) {
    var points = this.svg.select('#points')
    var x = this.xScales[this.xvar]
    var y = this.yScales[this.yvar]
    var xvar = this.xvar
    var yvar = this.yvar
    var xrun = this.xrun
    var yrun = this.yrun

    var new_URL = setParameterByName(window.location.href,'xrun',xrun)
    new_URL = setParameterByName(new_URL,'yrun',yrun)
    new_URL = setParameterByName(new_URL,'xvar',xvar)
    new_URL = setParameterByName(new_URL,'yvar',yvar)
    window.history.replaceState( {} , '', new_URL)

    console.log('plot X:', xvar, ' run', xrun, 'Y:', yvar, ' run', yrun)

    var xAxis = d3.axisBottom().scale(x).ticks(5)
    var yAxis = d3.axisLeft().scale(y).ticks(5).tickSize(0, 6)
    if (xvar == 'time'){xAxis.tickFormat(timeFormat)}
    var xpos = this.rect.height-20
    this.xaxis
	.attr('class', 'x axis')
	.attr('transform', 'translate(0,'+xpos+')')
	.call(xAxis)
    this.yaxis
	.attr('class', 'y axis')
	.attr('transform', 'translate(30,0)')
	.call(yAxis)
    if (xvar == 'well'){
	this.xaxis
	    .selectAll('text')
	    .style('text-anchor', 'end')
	    .attr('dx', '.2em')
	    .attr('dy', '-.4em')
	    .attr('transform', 'rotate(-65)')
    }

    var dynamics = this.dynamics
    points.selectAll('circle')
	.data(this.droplets)
	.transition().duration(duration)
	.attr('fill', function(d){return yvar in d ? timeseries.groupColor[d.group]:
				  dynamics[d.id_exp].values.filter(function(o){return +o.key==yrun}).length==1?
				  timeseries.groupColor[d.group]: 0})
	.attr('cx',function(d){return x(get_value(d, dynamics, xvar, xrun))})
	.attr('cy',function(d){return y(get_value(d, dynamics, yvar, yrun))})
	.attr('stroke',function(d){return d.selected ? 'black':null})
}

function distance(droplets){
    droplets.forEach(function(d){
	d['distance'] =  d['closest_inoculated'] - d['id_exp']
	d['distance_abs'] = Math.abs(d['closest_inoculated'] - d['id_exp'])
    })
    return droplets
}

function get_value(drop, dynamics, field, run){
    if (field in drop){
	return drop[field]
    }else{
	try{
	    var e = dynamics[drop.id_exp].values.filter(function(o){return +o.key==run})
	    return e[0].values[0][field]
	}
	catch(e) {
	    return undefined
	}
    }
}
