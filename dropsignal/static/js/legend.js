// legend.js - Write group color legend.
// This file is part of the dropsignal package.
// Copyright 2018 Guilhem Doulcier, Licence GNU AGPL3+
// ---

function write_group_legend(div, droplets, group_to_color){
    var nested_groups = d3.nest()
	.key(function(d) { return d.group })
	.entries(droplets)

    // Add a line per group.
    var grp = div.append('ul').selectAll('li')
	.data(nested_groups)
	.enter().append('li')
    grp.append('span')
	.text(function(d){return d.key +' ('+d.values.length+' droplets)' })
	.attr('class','label')
	.style('background-color',function(d){return group_to_color[d.key]})
}
