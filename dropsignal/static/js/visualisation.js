// visualisation.js - Droplet train visualisation
// This file is part of the dropsignal package.
// Copyright 2016 Guilhem Doulcier, Licence GNU AGPL3+
// ---
// The data is loaded by d3_queue, then the plots are setup in the ready() command.
// There is two main plots: the timeseries and the selector. Each one has a global variable associated.

///////////////////////////
// INIT GLOBAL VARIABLES //
///////////////////////////
var W = (19/20) * $(window).width(), H = 600 /// Window size

// Dataset
var data = {droplets:null, // droplets.csv will be stored here
	    dynamics:null, // dynamics.csv will be stored here
	    aggregated:null, // aggregated_dynamics will be stored here
	    meta:null // metadata.json will be stored here
	   }

// Store hidden/visible status for channels, droplets and wells [mapping str->bool].
var display_channel = {}, display_droplet = {}, display_well = {}, display_group = {}

// Timeseries chart data.
var timeseries = {
    xScale:null, yScale:null, // d3.scales
    dataContainer:null, // A d3 selection for the detached container (data are not attached to the DOM for perfomances.)
    chart:null, // Canvas object
    context:null, // Canvas plotter object
    data:null, // Nested timeseries, an array of objects with channel[str], droplet[str], values[array].
    channels: [], // Array of channels names, filled when loading droplet_dynamics.csv
    channelColor: {'Yellow_max':'#ff8300', 'RED_max': '#ff0000', 'YFP_max':'#00b200', LED2_max:'#ff0000', LED1_max:'#ff8300',Counting_max:'#00b200', Channel2_max:'#ff0000', Channel2_mean:'#ff0000', Channel1_max:'#00FF00', Channel1_mean:'#00FF00'}, // Some default colors.
    groupColor: {}, // The color of the preset selections
    colorMode: 'group', // Either channel or group.
    aggregationMode:'droplet',// Either droplet, well or group.
    stdMode: true,
    linewidth:.5,
    fontsize: 10
}

// Selector chart data
var dropselect = {
    coordinates:null, // Displayed coordinates.
    possible_coordinates: null, // Possible coordinates, collected from droplets.csv
    xScale:null, yScale:null, // d3.scales
    dataContainer:null, // A d3 selection for the detached container (data are not attached to the DOM for perfomances.)
    context:null, // foreground canvas plotter object
    background:null, // background canvas plotter object
    canvas:null, //foreground canvas object
    svg: null, // svg object (for axes brushing)
    coor_pos:null // coordinate position on the x-axis.
}


//////////////////////////////////////////////////////////////
// DATA LOADING - Load the different data files in parallel //
//////////////////////////////////////////////////////////////
d3_queue.queue()
    .defer(d3.csv, 'droplet_dynamics.csv', function(d) {
	if (+d.run != 0){
	var row = { time: +d.time,
		    id_exp: +d.id_exp,
		    run: +d.run}
	for (var key in d) {
	    if (key.split('_').pop()=='median'||key.split('_').pop()=='mean'||key.split('_').pop()=='area'||key=='size'||key=='speed'){
		row[key] = +d[key]
	    }
	}
	    return row}
	else{
	    return null
	}
    })
    .defer(d3.csv, 'aggregated_dynamics.csv', function(d) {
	if (+d.run != 0){

	var row = { time: +d.time_mean,
		    run: +d.run,
		    kind: d.kind,
		    key: d.key}
	for (var key in d) {
	    if (key.split('_').pop()=='median'||key.split('_').pop()=='std'||key.split('_').pop()=='mean'||key.split('_').pop()=='area'){
		row[key] = +d[key]
	    }
	}
	    return row}
	else{
	    return null
	}
    })
    .defer(d3.csv, 'droplet.csv', function(d) {
	var row = {
	    id_exp: +d.droplet,
	    well: d.well,
	    //tags: d.tags.replace(/'|\[|\]/gi, '').split(','),
	    group: d.group
	}
	for (var key in d) {
	    if (key.split('_').pop()=='median'||key.split('_').pop()=='mean'||key.split('_').pop()=='area'||key=='max_size'||key=='max_speed'){
		row[key] = +d[key]
	    }
	}
	return row
    })
    .defer(d3.json, 'metadata.json')
    .await(ready)

function ready(error, dynamics, aggregated, droplets, metadata) {
    /// THIS FUNCTION IS CALLED ONCE THE DATA IS LOADED AND SETUP THE WHOLE THING ///
    if(error) { console.log('Something went wrong:', error)}

    // Fill the data global variable.
    data.droplets = droplets
    data.dynamics = dynamics
    data.meta = metadata
    data.aggregated = aggregated

    console.log('Welcome in dropsignal !')
    console.log('Droplets data loaded: ', droplets.length)
    console.log('Dynamics points loaded:', dynamics.length)
    console.log('Metadata loaded: ', metadata)

    // Channel neame inference.
    timeseries.channels = []
    for (var key in dynamics[0]){if (key.split('_').pop()=='median'||key.split('_').pop()=='area'||key.split('_').pop()=='mean'){timeseries.channels.push(key)}}
    timeseries.channels.push('size')
    timeseries.channels.push('speed')

    console.log('Channels detected:', timeseries.channels)

    // Axes name inference, set some axes to visible based on the content of the timeseries.
    dropselect.possible_coordinates = {}
    for (var coord in droplets[0]){dropselect.possible_coordinates[coord] = false}
    timeseries.channels.forEach(function(chan){dropselect.possible_coordinates['max_'+chan] = true})
    dropselect.possible_coordinates['group'] = true
    dropselect.possible_coordinates['well'] = true
    dropselect.possible_coordinates['id_exp'] = true


    // Mappings
    data.meta['well_to_group'] = {}
    var groupscale =  d3.scale.category10()
    d3.map(data.droplets, function(d) { return d.group }).keys().forEach(function(d){timeseries.groupColor[d] = data.meta.color_group[d]?data.meta.color_group[d]:groupscale(d)})
    d3.map(data.droplets, function(d) { return d.well }).keys().forEach(function(d){display_well[d] = true})
    // Display default
    droplets.forEach(function(drop){display_droplet[drop.id_exp] = true,
				    data.meta.well_to_group[drop.well] = drop.group})
    timeseries.channels.forEach(function(channel){display_channel[channel] = false})
    display_channel[timeseries.channels[0]] = true // Display at least one channel

    // Setup the plots
    setup_dynamics(dynamics, aggregated)
    setup_selector(dropselect, droplets)
    setup_plate(droplets)
    setup_groups(droplets)
    setup_info(metadata)
    setup_template(metadata)
    setup_settings()

    // Draw everything
    draw_selector()
    draw_dynamics()
    draw_plate()

    // Activate the tooltips (bootstrapjs)
    $('[data-toggle="tooltip"]').tooltip()
}


//////////////////////////////////////////////////
// EXPERIMENT INFORMATIONS - The topleft column //
//////////////////////////////////////////////////

function setup_info(metadata){
    // Setup the topleft column

    // Add Buttons for reset selection and save plot.
    d3.select('#controls').append('button')
	.attr({'type':'button', 'class':'btn btn-primary btn-md',
	       'data-toggle':'tooltip', 'data-placement':'top',
	       'title':'First click: unselect everything, Second click: select everything'})
	.text('Reset selection')
	.on('click', clear)
    d3.select('#controls').append('a')
	.attr({'class':'btn btn-primary btn-md',
	       'data-toggle':'tooltip', 'data-placement':'top',
	       'title':'Download the current timeseries plot'})
	.text('Save plot')
	.style('margin-left','5px')
	.on('click', function(){this.href = timeseries.chart.node().toDataURL()
				this.download = 'timeseries.png'}, true)

    // Fill in experiment date and other metadata
    var info = d3.select('#info').append('div').attr('id', 'quick_select')
    var form = info.append('form').attr('class','navbar-form')
    form.append('label').text('Quick channel selection: ')
    var abs = form.append('select')
    abs.on('change',function(){var value = this.options[this.selectedIndex].value
			       timeseries.channels.forEach(function(chan){
				   display_channel[chan] = (chan == value)
			       })
			       d3.selectAll('.is_channel_visible').data(timeseries.channels)
			       .property('checked', function(d){return display_channel[d]})
			       draw_dynamics()
			      })
    abs.selectAll('option').data(timeseries.channels).enter().append('option')
	.attr('value',function(d){return d}).text(function(d){return d})


}

function clear(){
    var any = false
    for (var well in display_well){any = any || display_well[well]}
    for (well in display_well){display_well[well] = !any}
    for (var drop in display_droplet){display_droplet[drop] = !any}
    for (var group in display_group){display_group[group] = !any}

    draw_selector()
    draw_dynamics()
    draw_plate()
}

////////////////////////////////////////////////////////////////////
// TEMPLATE POPUP - Display the informations in metadata.droplets //
////////////////////////////////////////////////////////////////////

function setup_template(metadata){
    // Setup the template popup.
    var template = d3.select('#template div.modal-body').append('table').attr('class','table table-striped')

    function disp_list(list, sep=', '){
	// Join nested list in a string.
	if (Array.isArray(list)){
	    for(var i=0 ; i < list.length; i++){
		if ($.isArray(list[i])){
		    list[i] = list[i].join(sep)
		}
	    }
	    return list.join(sep)}
	else{return null}
    }

    // Create the table. One line per droplet group.
    template.html('<thead><tr><th>Label</th><th>Droplets</th><th>Expected Growth</th><th>Wells</th></tr></thead>')
    template.append('tbody').attr('id','droplets_groups_body')
    var table = d3.select('#droplets_groups_body')
    var tr = table.selectAll('tr')
	.data(metadata.droplets).enter()
	.append('tr')
    tr.append('th').html(function(m) { return m.label })
    tr.append('td').append('div').attr('class', 'scroll').html(function(m) { return disp_list(m.id) })
    tr.append('td').html(function(m) { return m.expected_growth })
    tr.append('td').append('div').attr('class', 'scroll').html(function(m) { return disp_list(m.well)})
}

///////////////////////////////////////////////////////////
// SETTINGS POPUP - Set the channels and axes to display //
///////////////////////////////////////////////////////////

function setup_settings(){
    // Redraw when the popup is closed.
    $('#settings').on('hidden.bs.modal', function () {draw_selector(); draw_dynamics(); draw_plate()})
    var settings = d3.select('#settings div.modal-body')


    //////////////////////////////
    // Timeseries channel setup //
    //////////////////////////////
    var chanset = settings.append('form').attr('id','channel_setting').attr('class','form-inline container-fluid')
    chanset.append('h3').text('Timeseries Channels')
    chanset.append('p').text('Select the channels to display in the time-series. Settings are automatically updated when you close this dialog.')
    chanset.append('div').attr('class','row').html('<strong class="col-md-2"></strong><div class="col-md-2">Color</div><div class="col-md-3">Min</div><div class="col-md-3">Max</div><div class="col-md-2">Visible</div>')

    // Add a row to control the TIME scale.
    var time = chanset.append('div').attr('class','row')
    time.append('span').attr('class','col-md-4').text('Time')
    time.append('label').attr('class','col-md-3')
	.append('input').attr('type', 'number').attr('class','form-control')
	.attr('value', timeseries.min['time'])
	.attr('max', timeseries.max['time'])
	.on('change', function(){var domain = timeseries.xScale.domain()
				  timeseries.xScale.domain([d3.select(this).property('value'),domain[1]])})
    time.append('label').attr('class','col-md-3')
	.append('input').attr('type', 'number').attr('class','form-control')
	.attr('value',timeseries.max['time'])
	.attr('min', timeseries.min['time'])
	.on('change', function(){var domain = timeseries.xScale.domain()
				  timeseries.xScale.domain([domain[0],d3.select(this).property('value')])})

    // Add a row for each channel
    var chan = chanset.selectAll('div.channel')
	.data(timeseries.channels)
	.enter().append('div').attr('class','row channel')
    chan.append('span').attr('class','col-md-2').text(function(d){return d+': '})
    chan.append('label').attr('class','col-md-2')
	.append('input').attr('type', 'color').attr('class','form-control').style('width','3em')
	.attr('value',function(d){return timeseries.channelColor[d] || '#000000'})
	.on('change', function(d){timeseries.channelColor[d] = d3.select(this).property('value')})
    chan.append('label').attr('class','col-md-3')
	.append('input').attr('type', 'number').attr('class','form-control')
	.attr('value',function(d){return timeseries.min[d]})
	.attr('max',  function(d){return timeseries.max[d]})
	.on('change', function(d){var domain = timeseries.yScale[d].domain()
				  timeseries.yScale[d].domain([d3.select(this).property('value'),domain[1]])})
    chan.append('label').attr('class','col-md-3')
	.append('input').attr('type', 'number').attr('class','form-control')
	.attr('value',function(d){return timeseries.max[d]})
	.attr('min',  function(d){return timeseries.min[d]})
	.on('change', function(d){var domain = timeseries.yScale[d].domain()
				  timeseries.yScale[d].domain([domain[0],d3.select(this).property('value')])})
    chan.append('label').attr('class','col-md-2')
	.append('input').attr('type','checkbox').attr('class','is_channel_visible')
	.property('checked', function(d){return display_channel[d]})
	.on('change', function(d){display_channel[d]=this.checked})

    // Line width and font size //
    var lw = settings.append('form').attr('class','form-inline container-fluid')
    lw.append('label').text('Line Width: ').style('display','inline')
    var lw_disp = lw.append('span').attr('id','linewidth').text(timeseries.linewidth).style('display','inline')
    lw.append('input').attr('type', 'range').attr('max',3).attr('min',0).attr('step',0.01)
	.style('display','inline').style('width','30%').attr('value',  timeseries.linewidth)
	.on('change', function(){timeseries.linewidth = this.value; lw_disp.html(this.value)})
    lw.append('label').text('Font size: ').style('display','inline')
    var fs_disp = lw.append('span').attr('id','fontsize').text(timeseries.fontsize).style('display','inline')
    lw.append('input').attr('type', 'range').attr('max',30).attr('min',0).attr('step',1)
	.style('display','inline').style('width','30%').attr('value',  timeseries.fontsize)
	.on('change', function(){timeseries.fontsize = this.value; fs_disp.html(this.value)})


    /// Color mode ///

    var colorMode = settings.append('form').attr('class','form-inline container-fluid')
    colorMode.append('h4').text('Color Mode')
    colorMode.append('p').text('Select the way to color the timeseries.')
    colorMode.append('label').attr('class','col-md-3').text('Channel: ')
	.append('input').attr('type','radio').attr('value','channel').attr('name','colorMode')
	.property('checked', timeseries.colorMode == 'channel')
	.on('change', function(){timeseries.colorMode = d3.select(this).property('value')})
    colorMode.append('label').attr('class','col-md-3').text('Groups: ')
	.append('input').attr('type','radio').attr('value','group').attr('name','colorMode')
	.property('checked', timeseries.colorMode == 'group')
	.on('change', function(){timeseries.colorMode = d3.select(this).property('value')})

    /// Aggregation ///
    var aggregationMode = settings.append('form').attr('class','form-inline container-fluid')
    aggregationMode.append('h4').text('Aggregation Mode')
    aggregationMode.append('p').text('Select the way the timeseries are aggregated.')
    aggregationMode.append('label').attr('class','col-md-3').text('Droplet: ')
	.append('input').attr('type','radio').attr('value','droplet').attr('name','aggregationMode')
	.property('checked', timeseries.aggregationMode == 'droplet')
	.on('change', function(){
	    if ((timeseries.aggregationMode == 'group' &&timeseries.linewidth == 1.5)
	       || (timeseries.aggregationMode == 'well' && timeseries.linewidth == 1.5))
	    {timeseries.linewidth = .5; lw_disp.html(.5)}
	    timeseries.aggregationMode = d3.select(this).property('value')
	   })
    d3.map(timeseries.data_aggregated, function(d){return d.kind}).keys().forEach(function(kind){
	aggregationMode.append('label').attr('class','col-md-3').text(kind[0].toUpperCase()+kind.slice(1)+': ')
	    .append('input').attr('type','radio').attr('value',kind).attr('name','aggregationMode')
	    .property('checked', timeseries.aggregationMode == kind)
	    .on('change', function(){
		if (timeseries.aggregationMode == 'droplet' && timeseries.linewidth == 0.5)
		{timeseries.linewidth = 1.5; lw_disp.html(1.5)}
		timeseries.aggregationMode = d3.select(this).property('value')
	    })
    })
    aggregationMode
	.append('label').attr('class','col-md-6').text('Error bar (+/- standard deviation): ')
	.append('input').attr('type','checkbox')
	.property('checked', timeseries.stdMode)
	.on('change', function(){timeseries.stdMode = d3.select(this).property('checked')})

    ///////////////////////
    // Selector settings //
    ///////////////////////
    var set_selector = settings.append('div')
    set_selector.append('form').attr('id','selector_setting').attr('class','form-inline container-fluid')
    set_selector.append('h3').text('Selector settings')
    set_selector.append('p').text('Select the variable to use in the selector. You need to re-initialise the selector to take into account any change (be careful, you will loose your current selection !)')

    // Add a checkbox for each possible coordinate.
    var coord = set_selector.append('div').attr('class','row').selectAll('div.formgroup')
	.data(d3.keys(dropselect.possible_coordinates))
	.enter()
    coord.append('label').attr('class','col-md-3').text(function(d){return d+': '})
	.append('input').attr('type','checkbox')
	.property('checked', function(d){return dropselect.possible_coordinates[d]})
	.on('change', function(d){dropselect.possible_coordinates[d]=this.checked})

    // Reset selector button.
    set_selector.append('button')
	.attr({type:'button', 'class':'btn btn-danger'})
	.text('Reset the selector')
	.on('click', function(){
	    for (var well in display_well){display_well[well] = true}
	    for (var drop in display_droplet){display_droplet[drop] = true}
	    setup_selector(dropselect, data.droplets)
	    $('#settings').modal('hide')})
}

///////////////////////////////////////////////
// PLATE -- Quick droplet selection per well //
///////////////////////////////////////////////

function setup_plate(droplets){

    // Setup the plate sketch
    var plate = d3.select('#plate svg')
	.attr('width', W/4).attr('height', H/2.5)
    var rows = d3.map(droplets, function(d){return d.well[0]}).keys()
    var columns = d3.map(droplets, function(d){return parseInt(d.well.substring(1))}).keys()

    var scale_col = d3.scale.ordinal().rangePoints([0,plate.attr('width')],3).domain(columns)
    var scale_row = d3.scale.ordinal().rangePoints([0,plate.attr('height')],3).domain(rows)
    var r = 0.45 * Math.min(Math.abs(scale_col.range()[1]-scale_col.range()[0]),Math.abs(scale_row.range()[1]-scale_row.range()[0]))

    var nested_well = d3.nest()
	.key(function(d) { return d.well })
	.entries(droplets)
    // Display the current well
    var tooltip = plate.append('text')
	.attr({x:scale_col.range()[0]/2, y:scale_row.range()[0]/2, 'alignment-baseline':'middle', 'style':'text-anchor: middle'})


    // Add one circle per well.
    plate.selectAll('circle')
	.data(nested_well)
	.enter().append('circle')
	.attr('class', 'well')
	.attr('cy', function(d) { return scale_row(d.key[0])})
	.attr('cx', function(d) { return scale_col(d.key.substring(1))})
	.attr('r', r)
	.on('mouseover', function(d) {tooltip.transition(200).style('opacity', .9).text(d.key)})
	.on('mouseout', function() {tooltip.transition(200).style('opacity', 0)})
	.on('click',brush_well)

    // Add rows and columns symbols that can select when clicked
    var row_symbols = plate.append('g')
	.attr('id','row_symbols')
	.selectAll('g')
	.data(rows).enter().append('g')
	.attr('transform', function(d) { return 'translate(' + 10 + ',' + scale_row(d) + ')'})
	.on('click',brush_row)
    row_symbols	.append('path')
	.style('opacity','.10')
	.attr('transform',' rotate(90)')
	.attr('d', d3.svg.symbol().type('triangle-up'))
	.style('fill', 'black')
    row_symbols.append('text')
	.attr({'alignment-baseline':'bottom', 'style':'text-anchor: middle'})
	.text(function(d){return d})
	.style('opacity','.6')

    var col_symbols = plate.append('g')
	.attr('id','col_symbols')
	.selectAll('g')
	.data(columns).enter().append('g')
	.attr('transform', function(d) { return 'translate(' + scale_col(d) + ',' +15  + ')'})
	.on('click',brush_col)
    col_symbols.append('path')
	.attr('d', d3.svg.symbol().type('triangle-down'))
	.style('fill', 'black')
	.style('opacity','.10')
    col_symbols.append('text')
	.attr({'alignment-baseline':'middle', 'style':'text-anchor: middle'})
	.text(function(d){return d})
	.style('opacity','.6')

    var nested_row = d3.nest()
	.key(function(d) { return d.well[0] })
	.entries(droplets)
    var nested_col = d3.nest()
	.key(function(d) { return d.well.substring(1) })
	.entries(droplets)

    function brush_row(row){
	var wells = d3.keys(display_well).filter(function(well){return well[0] == row})
	var display = wells.every(function(well){return display_well[well]}) ? false : true
	wells.forEach(function(well){display_well[well] = display})
	nested_row.filter(function(d){return d.key==row})[0].values.forEach(function(drop){display_droplet[drop.id_exp] = display})
	draw_selector()
	draw_dynamics()
	draw_plate()
    }

    function brush_col(col){
	var wells = d3.keys(display_well).filter(function(well){return well.substring(1) == col})
	var display = wells.every(function(well){return display_well[well]}) ? false : true
	wells.forEach(function(well){display_well[well] = display})
	nested_col.filter(function(d){return d.key==col})[0].values.forEach(function(drop){display_droplet[drop.id_exp] = display})
	draw_selector()
	draw_dynamics()
	draw_plate()
    }

    function brush_well(well){
	// Toogle well visibility and update the visibility of associated droplets and redraw.
	if (timeseries.aggregationMode != 'group'){
	    display_well[well.key] = !display_well[well.key]
	    droplets.forEach(function(drop){if (drop.well==well.key){display_droplet[drop.id_exp] = display_well[well.key]}})
	    draw_selector()
	    draw_dynamics()
	    draw_plate()
	}
    }
}

function draw_plate(){
    // Update the well color according to their selection status
    var plate = d3.select('#plate svg')
    plate.selectAll('circle')
	.style('fill', function(d) {return display_well[d.key] ? timeseries.groupColor[data.meta.well_to_group[d.key]] : 'rgba(0,0,0,0.1)'})
}

////////////////////////////////////////
//  GROUPS -- Quick droplet selection //
////////////////////////////////////////
function setup_groups(droplets){
    // Setup the topright column, group selector.
    var group_col = d3.select('#groups').style('max-height', H/2.5)
    var nested_groups = d3.nest()
	.key(function(d) { return d.group })
	.entries(droplets)

    nested_groups.forEach(function(d){display_group[d.key]=true})

    // Add a line per group.
    var grp_list = group_col.append('ul')
    var grp = grp_list.selectAll('li')
	.data(nested_groups)
	.enter().append('li')
    // -- Link Selector
    grp.append('a')
	.text(function(d){return d.key +' ('+d.values.length+' droplets)' })
	.on('click', brush_group)
    // -- Color picker
    grp.append('input').attr('type', 'color').attr('class','form-control').style('margin-left','1em')
	.attr('value',function(d){return timeseries.groupColor[d.key]})
	.on('change', function(d){timeseries.groupColor[d.key] = d3.select(this).property('value');  draw_dynamics(); draw_plate()})

    function brush_group(group){
	// Selection tool for groups.

	// If every single well is selected already. Deselect everything.
	var any = true
	for (var well in display_well){any = any && display_well[well]}
	if (any == true){
	    for (well in display_well){display_well[well] = false}
	    for (var drop in display_droplet){display_droplet[drop] = false}
	    for (var g in display_group){display_group[g] = false}
	}

	// Select droplets that belong to a group, and redraw.
	group.values.forEach(function(d){display_droplet[d.id_exp]=true; display_well[d.well]=true})
	display_group[group.key] = true

	draw_selector()
	draw_dynamics()
	draw_plate()
    }
}

////////////////////////////////////////////////////
//  DYNAMICS PLOT - This plot show the timeseries //
////////////////////////////////////////////////////

function extract_timeseries(dynamics, channels){
    // Extract an array of timeseries {droplet[str], channel[str], values[Array]}.
    var data = []
    var nested_data = d3.nest()
	.key(function(d) { return d.id_exp })
	.entries(dynamics)
    nested_data.forEach(function(drop){
	channels.forEach(function(channel){
	    data.push({'droplet':drop['key'],
		       'channel':channel,
		       'values': drop.values.map( function(d){return {'t': d['time'], 'v':d[channel]} })
		      })
	})
    })
    return data
}

function extract_aggregated_timeseries(dynamics, channels){
    // Extract an array of timeseries {droplet[str], channel[str], values[Array], std[Array]}.
    var data = []
    var nested_data = d3.nest()
	.key(function(d) { return d.kind })
	.key(function(d) { return d.key })
	.entries(dynamics)
    nested_data.forEach(function(kind){
	kind.values.forEach(function(key){
	    channels.forEach(function(channel){
		data.push({'kind':kind.key,
			   'key':key.key,
			   'channel':channel,
			   'values': key.values.map( function(d){return {'t': d['time'], 'v':d[channel+'_mean']}}),
			   'std': key.values.map( function(d){return {'t': d['time'], 'v':d[channel+'_std']} })
			  })
	    })
	})
    })
    return data
}

function setup_dynamics(dynamics, aggregated_dynamics) {
    // Setup the timeseries plot.
    var row = d3.select('#dynamics')
    timeseries.chart = row.select('canvas')
	.attr({'width':W, 'height':H})
    timeseries.context = timeseries.chart.node().getContext('2d')

    // Setup a linear scale for each channel.
    timeseries.yScale = {}
    timeseries.min = {}
    timeseries.max = {}
    timeseries.channels.forEach(function(channel){
	timeseries.min[channel] = d3.min(dynamics, function(d) {return d[channel]})
	timeseries.max[channel] = d3.max(dynamics, function(d) {return d[channel]})
	timeseries.yScale[channel] = d3.scale.linear().range([H-12,0]).domain([timeseries.min[channel],timeseries.max[channel]])
    })

    // Time scale.
    timeseries.min['time'] = d3.min(dynamics,function(d) {return d.time})
    timeseries.max['time'] = d3.max(dynamics,function(d) {return d.time})
    timeseries.xScale = d3.scale.linear().range([10,W]).domain([timeseries.min.time,timeseries.max.time])

    // Bind the data to a container that will NOT be attached to the DOM
    // Thus allowing to display a lot of timeseries at a time without trashing the browser.
    timeseries.data = extract_timeseries(dynamics, timeseries.channels) // Convert the data in an array of timeseries.
    var detachedContainer = document.createElement('dynamics_elements')
    timeseries.dataContainer = d3.select(detachedContainer)
    var dataBinding = timeseries.dataContainer.selectAll('custom')
	.data(timeseries.data)
    dataBinding.enter()
	.append('custom')

    timeseries.data_aggregated = extract_aggregated_timeseries(aggregated_dynamics, timeseries.channels)
    timeseries.dataContainer.selectAll('aggregated')
	.data(timeseries.data_aggregated)
	.enter()
	.append('aggregated')
}

function draw_dynamics() {
    // Draw the timeseries.
    var context = timeseries.context
    context.clearRect(0,0,W,H) // Clear canvas

    // Select the timeseries to display.
    var elements

    if (timeseries.aggregationMode == 'droplet'){
	elements = timeseries.dataContainer.selectAll('custom').
	    filter(function(d){return display_droplet[d.droplet] && display_channel[d.channel]})
    } else if (timeseries.aggregationMode == 'well') {
	elements = timeseries.dataContainer.selectAll('aggregated').
	    filter(function(d){return d.kind=='well' && display_channel[d.channel] && display_well[d.key]})
    } else if (timeseries.aggregationMode == 'group') {
	elements = timeseries.dataContainer.selectAll('aggregated').
	    filter(function(d){return d.kind=='group' && display_channel[d.channel] && display_group[d.key]})
    }

    // Display them.
    context.lineWidth= timeseries.linewidth
    context.font = timeseries.fontsize + 'px Arial'
    elements.each(function() {
	var node =  d3.select(this)
	var datum = node.datum()

	context.beginPath()
	if (timeseries.colorMode == 'group'){
	    if (timeseries.aggregationMode == 'well'){
		context.strokeStyle = timeseries.groupColor[data.meta['well_to_group'][datum.key]] || '#000000'
	    }else if (timeseries.aggregationMode == 'group'){
		context.strokeStyle = timeseries.groupColor[datum.key] || '#000000'
	    }
	    else{
		context.strokeStyle = timeseries.groupColor[data.droplets[datum.droplet].group] || '#000000'
	    }
	}else{
	    context.strokeStyle = timeseries.channelColor[datum.channel]
	}

	context.moveTo(	timeseries.xScale(datum.values[0]['t']), timeseries.yScale[datum.channel](datum.values[0]['v']))
	datum.values.forEach(function(val){
	    context.lineTo(timeseries.xScale(val['t']), timeseries.yScale[datum.channel](val['v']))
	})
	if (timeseries.stdMode && 'std' in datum){
	    for (var i = 0; i < datum.values.length; i++){
		context.moveTo(	timeseries.xScale(datum.values[i]['t']), timeseries.yScale[datum.channel](datum.values[i]['v']+datum.std[i]['v']))
		context.lineTo(	timeseries.xScale(datum.values[i]['t']), timeseries.yScale[datum.channel](datum.values[i]['v']-datum.std[i]['v']))
	    }
	}
	if (elements[0].length<21){
	    var text = timeseries.aggregationMode == 'droplet' ? datum.droplet : datum.key
	    var time = Math.max(Math.min(Math.floor(datum.values.length / 3 + Math.random()*10), datum.values.length-1),0)
	    context.fillStyle = context.strokeStyle
	    context.textAlign='center'
	    context.fillText(text,
			     timeseries.xScale(datum.values[time]['t']),
			     timeseries.yScale[datum.channel](datum.values[time]['v']))
	}
	context.stroke()
    })

    // Draw the Y-axes (channels).
    var offset = 10
    var channels = timeseries.channels.filter(function(channel){return display_channel[channel]})
    var L = channels.length
    context.lineWidth='.5'
    channels.forEach(function(channel, i){
	var ticks = timeseries.yScale[channel].ticks()
	var color = timeseries.colorMode == 'channel' ? timeseries.channelColor[channel]:'#000000'
	var x0 = timeseries.xScale(timeseries.xScale.domain()[0])+offset

	// Axis Spine
	context.strokeStyle = color
	context.fillStyle = color
	context.textAlign='left'
	context.beginPath()
	context.moveTo(x0, timeseries.yScale[channel](ticks[0]))
	context.lineTo(x0, timeseries.yScale[channel](ticks[ticks.length-1]))
	context.stroke()

	// Axis Name
	context.fillText(channel,10*L+30,timeseries.yScale[channel](ticks[ticks.length-1])+(i+1)*timeseries.fontsize)

	// Axis ticks
	ticks.forEach(function(tick){
	    context.fillText(tick,x0,timeseries.yScale[channel](tick)+timeseries.fontsize)
	    context.setLineDash([5,2])
	    context.beginPath()
	    context.moveTo(x0, timeseries.yScale[channel](tick))
	    context.lineTo(W, timeseries.yScale[channel](tick))
	    context.stroke()
	    context.setLineDash([0])
	})
    })

    // Draw the time axis
    context.fillStyle = '#000000'
    context.strokeStyle = '#000000'
    context.textAlign='center'
    context.beginPath()
    context.moveTo(0, H-5)
    context.lineTo(W, H-5)
    context.stroke()
    function tickfmt(d){var h=Math.floor(d/60);	return Math.floor(h/60)+':'+h%60+':'+d%60}
    timeseries.xScale.ticks().forEach(function(tick){
	context.fillText(tickfmt(tick),timeseries.xScale(tick),H-12)
	context.beginPath()
	context.moveTo(timeseries.xScale(tick), H)
	context.lineTo(timeseries.xScale(tick), H-11)
	context.stroke()
    })
    context.font = '10px Arial'
    context.textAlign='right'
    context.fillText('Time ', W,H-5)
    context.textAlign='left'
    context.font = timeseries.fontsize + 'px Arial'

}


///////////////////////////////////////////////////////////////////
// SELECTOR - A parallel coordinate plot to filter the droplets. //
///////////////////////////////////////////////////////////////////

// This one is a fairly complicated stacking of:
//
// - A background canvas displaying all droplets in transparent black,
// - A foreground canvas displaying selected droplets in color,
// - A svg element containing only the axis and the brushes.
//
// For performance reasons, the data and their representation are not
// attached to the DOM.

function setup_selector(selector, droplets){
    // Setup the selector plot
    var row = d3.select('#selector_wrapper')
    d3.select('#selector').attr('style','height:'+(H+100)+'px;')
    selector.canvas = row.select('#foreground')
    var background = row.select('#background')
    selector.svg = row.select('svg')
    selector.context = selector.canvas.node().getContext('2d')
    selector.background = background.node().getContext('2d')

    // Set size and position: We want the two canvas and the svg to be exactly alined.
    row.attr({style:'position: relative; margin:auto; width:'+W+'px;'})
    selector.svg.attr({'width':W, 'height':H, style: 'position: absolute;top: 0; left: 0;   z-index: 2;'})
    selector.canvas.attr({'width':W, 'height':H,style: 'position: absolute;top: 0; left: 0;   z-index: 1;'})
    background.attr({'width':W, 'height':H,style: 'position: absolute;top: 0; left: 0;   z-index: 0;'})

    ///////////////////////
    // Coordinates setup //
    ///////////////////////
    // Build the list of coordinates we will use based on the settings.
    selector.coordinates = []
     for (var coord in selector.possible_coordinates){
	 if(selector.possible_coordinates[coord] == true){
	     selector.coordinates.push(coord)
	 }
     }
    selector.coordinates = selector.coordinates.sort()

    // Setup a scale for every coordinate
    selector['yScale'] = {}
    selector['coor_pos'] = {}
    selector.xScale = d3.scale.ordinal().domain(selector.coordinates).rangePoints([20, W-20])
    selector.coordinates.forEach(function(key){
	if (key == 'group' || key == 'well'){
	    selector.yScale[key] = d3.scale.ordinal().rangePoints([H-15,15]).domain(droplets.map( function (d) { return d[key]}))
	} else{
	    selector.yScale[key] = d3.scale.linear().range([H-5,12]).domain([d3.min(droplets,function(d) {return d[key]}),
									     d3.max(droplets,function(d) {return d[key]})])
	}
	selector.coor_pos[key] = selector.xScale(key)
    })
    //////////////////
    // Data binding //
    //////////////////
    // Bind the data to a container that will NOT be attached to the DOM
    // Thus allowing to display a lot of timeseries at a time without trashing the browser.
    var detachedContainer = document.createElement('dynamics_elements')
    selector['dataContainer'] = d3.select(detachedContainer)
    var dataBinding = selector.dataContainer.selectAll('custom')
	.data(droplets)
    dataBinding.enter()
	.append('custom')

    // Display the background once and for all.
    parallel_coordinates(selector.dataContainer.selectAll('custom'), selector.background, 'rgba(0,0,0,0.1)')

    ///////////////
    // SVG setup //
    ///////////////
    // Clear the svg
    selector['svg'].selectAll('*').remove()

    // Draw an axis on the SVG for each coordinate and attach the brush event.
    var axis = d3.svg.axis()
    var g = selector.svg.selectAll('.dimension')
	.data(selector.coordinates)
	.enter().append('svg:g')
	.attr('class', 'dimension')
	.attr('transform', function(d) { return 'translate(' + selector.xScale(d) + ')' })

    g.append('svg:g')
	.attr('class', 'axis')
	.each(function(d)
	      {d3.select(this).call(axis
				    .orient(selector.xScale(d)==selector.xScale.range()[0]?'right':'left')
				    .scale(selector.yScale[d]))})
	.append('svg:text')
	.attr('text-anchor', 'middle')
	.attr('y', 10)
	.text(String)
	.call(d3.behavior.drag() // Reorder axis on drag'n'drop
	      .origin(function(d) { return d })
	      .on('dragstart', function(d) {
		  selector.coor_pos[d] = selector.xScale(d)
	      })
	      .on('drag', function(d) {
		  selector.coor_pos[d] = Math.min(W-10, Math.max(10, selector.coor_pos[d]+d3.event.dx))
		  g.filter(function(el){return el==d}).attr('transform', function(d) { return 'translate(' + selector.coor_pos[d] + ')' })
	      })
	      .on('dragend', function() {
		  selector.coordinates = selector.coordinates.sort(function(a, b) { return selector.coor_pos[a] - selector.coor_pos[b] })
		  selector.xScale.domain(selector.coordinates)
		  g.attr('transform', function(d) { return 'translate(' + selector.xScale(d) + ')' })
		  parallel_coordinates(selector.dataContainer.selectAll('custom'), selector.background, 'rgba(0,0,0,0.1)')
		  draw_selector()
	      })
	     )

    g.append('svg:g')
	.attr('class', 'brush')
	.each(function(d) {
	    if (d != 'group' && d != 'well'){
		d3.select(this).call(selector.yScale[d].brush = d3.svg.brush().y(selector.yScale[d]).on('brush', brush))}
	    else{selector.yScale[d].brush = {empty:function(){return true}}}
	})
	.selectAll('rect')
	.attr('x', -8)
	.attr('width', 16)

    function brush() {
	// Filter the displayed dataset according to the selector's brushes. And redraw.
	var actives = selector.coordinates.filter(function(p) { return !selector.yScale[p].brush.empty()})
	var extents = actives.map(function(p) { return selector.yScale[p].brush.extent() })
	for (var well in display_well){display_well[well]=false}
	droplets.forEach(function(datum){
	    display_droplet[datum.id_exp] = actives.every(function(p, i) {
		return extents[i][0] <= datum[p] && datum[p] <= extents[i][1]})
	    if (display_droplet[datum.id_exp] == true){display_well[datum.well] = true}
	})
	draw_selector()
	draw_dynamics()
	draw_plate()
    }
}

function draw_selector(){
    // Draw the colored lines for selected droplets on the foreground canvas.
    var drops = dropselect.dataContainer.selectAll('custom')
	.filter(function(d){return display_droplet[d.id_exp]})
    parallel_coordinates(drops, dropselect.context,'multi' )
}

function parallel_coordinates(drops, ctx, color){
    // Draw the parallel coordinate plots.
    ctx.clearRect(0,0,W+1,H+1)
    ctx.lineWidth='.1'
    ctx.strokeStyle = color
    var c0 = dropselect.xScale.domain()[0]
    drops.each(function(datum){
	ctx.beginPath()
	if (color == 'multi'){
	    ctx.strokeStyle = timeseries.groupColor[datum.group]
	}
	ctx.moveTo(dropselect.xScale(c0), dropselect.yScale[c0](datum[c0]))
	dropselect.coordinates.forEach(function(coor){
	    ctx.lineTo(dropselect.xScale(coor), dropselect.yScale[coor](datum[coor]))
	})
	ctx.stroke()
    })
}
