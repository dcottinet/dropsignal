// Scatter.js
// This file is part of the dropsignal package.
// Copyright 2016-2018 Guilhem Doulcier, Licence GNU AGPL3+
// ---

// Dependencies
var Scatter //from scatter.js
var Plate // from plate.js
var DropletSelector // from selector.js
var write_group_legend // from legend.js

///////////////////////////
// INIT GLOBAL VARIABLES //
///////////////////////////
var W = (19.5/20) * $(window).width(), H = 600 /// Window size

var scatter
var selector
var plate

// Dataset
var data = {droplets:null, // droplets.csv will be stored here
	    dynamics:null, // dynamics.csv will be stored here
	    aggregated:null, // aggregated_dynamics will be stored here
	    meta:null // metadata.json will be stored here
	   }

// Store hidden/visible status for channels, droplets and wells [mapping str->bool].
var display_channel = {}, display_droplet = {}, display_well = {}

// Timeseries chart data.
var timeseries = {
    xScale:null, yScale:null, // d3.scales
    dataContainer:null, // A d3 selection for the detached container (data are not attached to the DOM for perfomances.)
    chart:null, // Canvas object
    context:null, // Canvas plotter object
    data:null, // Nested timeseries, an array of objects with channel[str], droplet[str], values[array].
    channels: [], // Array of channels names, filled when loading droplet_dynamics.csv
    channelColor: {'Yellow_max':'#ff8300', 'RED_max': '#ff0000', 'YFP_max':'#00b200', LED2_max:'#ff0000', LED1_max:'#ff8300',Counting_max:'#00b200', Channel2_max:'#ff0000', Channel2_value:'#ff0000', Channel1_max:'#00FF00', Channel1_value:'#00FF00'}, // Some default colors.
    groupColor: {}, // The color of the preset selections
    colorMode: 'group', // Either channel or group.
    aggregationMode:'droplet',// Either droplet, well or group.
    stdMode: true,
    linewidth:.5
}

// Selector chart data
var dropselect = {
    coordinates:null, // Displayed coordinates.
    possible_coordinates: null, // Possible coordinates, collected from droplets.csv
    xScale:null, yScale:null, // d3.scales
    dataContainer:null, // A d3 selection for the detached container (data are not attached to the DOM for perfomances.)
    context:null, // foreground canvas plotter object
    background:null, // background canvas plotter object
    canvas:null, //foreground canvas object
    svg: null, // svg object (for axes brushing)
    coor_pos:null // coordinate position on the x-axis.
}


//////////////////////////////////////////////////////////////
// DATA LOADING - Load the different data files in parallel //
//////////////////////////////////////////////////////////////
d3.queue()
    .defer(d3.csv, 'droplet_dynamics.csv', function(d) {
	var row = { time: +d.time,
		    size: +d.size,
		    speed: +d.speed,
		    id_exp: +d.id_exp,
		    run: +d.run}
	for (var key in d) {
	    if (key.split('_').pop()=='max'||key.split('_').pop()=='mean'||key.split('_').pop()=='std'||key.split('_').pop()=='cov'||key.split('_').pop()=='median'||key.split('_').pop()=='area'){
		row[key] = +d[key]
	    }
	}
	return row
    })
    .defer(d3.csv, 'aggregated_dynamics.csv', function(d) {
	var row = { time: +d.time_mean,
		    run: +d.run,
		    kind: d.kind,
		    key: d.key}
	for (var key in d) {
	    if (key.split('_').pop()=='max'||key.split('_').pop()=='mean'){
		row[key] = +d[key]
	    }
	}
	return row
    })
    .defer(d3.csv, 'droplet.csv', function(d) {
	var row = {
	    id_exp: +d.droplet,
	    well: d.well,
	    expected_growth: +d.expected_growth,
	    grown: +d.grown,
	    closest_inoculated: +d.closest_inoculated,
	    group: d.group,
	    tags: d.tags.replace(/'|\[|\]/gi, '').split(',')
	}
	for (var key in d) {
	    if (key.split('_').pop()=='max'||key.split('_').pop()=='mean',key.split('_').pop()=='shift'){
		row[key] = +d[key]
	    }
	}
	return row
    })
    .defer(d3.json, 'metadata.json')
    .await(ready)

function ready(error, dynamics, aggregated, droplets, metadata) {
    /// THIS FUNCTION IS CALLED ONCE THE DATA IS LOADED AND SETUP THE WHOLE THING ///
    if(error) { console.log('Something went wrong:', error)}

    // Fill the data global variable.
    data.droplets = droplets
    data.dynamics = dynamics
    data.meta = metadata
    data.aggregated = aggregated

    console.log('Welcome in dropsignal !')
    console.log('Droplets data loaded: ', droplets.length)
    console.log('Dynamics points loaded:', dynamics.length)
    console.log('Metadata loaded: ', metadata)

    // Channel neame inference.
    timeseries.channels = []
    for (var key in dynamics[0]){if (key.split('_').pop()=='max'||key.split('_').pop()=='value'){timeseries.channels.push(key)}}
    console.log('Channels detected:', timeseries.channels)

    // Axes name inference, set some axes to visible based on the content of the timeseries.
    dropselect.possible_coordinates = {}
    for (var coord in droplets[0]){dropselect.possible_coordinates[coord] = false}
    timeseries.channels.forEach(function(chan){dropselect.possible_coordinates['max_'+chan] = true})
    dropselect.possible_coordinates['group'] = true
    dropselect.possible_coordinates['well'] = true
    dropselect.possible_coordinates['id_exp'] = true


    // Mappings
    data.meta['well_to_group'] = {}
    data.meta['well_to_color'] = {}
    var groupscale =  d3.scaleOrdinal(d3.schemeCategory10)
    d3.map(data.droplets, function(d) { return d.group }).keys().forEach(function(d){timeseries.groupColor[d] = data.meta.color_group[d]?data.meta.color_group[d]:groupscale(d)})
    d3.map(data.droplets, function(d) { return d.well }).keys().forEach(function(d){display_well[d] = true})
    droplets.forEach(function(drop){display_droplet[drop.id_exp] = true,
				    data.meta.well_to_group[drop.well] = drop.group
				    data.meta.well_to_color[drop.well] = data.meta.color_group[drop.group]})

    d3.select('#scatter').attr('width',W).attr('height',3*H/4).style('margin-left','10px')
    plate = new Plate(d3.select('#plate').append('svg'), 500, 300, droplets, data.meta['well_to_color'])
    selector = new DropletSelector(droplets, d3.select('#segments'), plate)
    scatter = new Scatter(droplets, dynamics,d3.select('#scatter'),d3.select('#conta_control'),selector)
    write_group_legend(d3.select('#legend'), droplets, data.meta.color_group)

    plate.click_well = function(d){selector.select_well(d.key); scatter.draw()}
    plate.click_row = function(d){selector.select_row(d); scatter.draw()}
    plate.click_column = function(d){selector.select_column(d); scatter.draw()}
}
