// raw_explorer.js - Raw signal visualization
// This file is part of the dropsignal package.
// Copyright 2017 Guilhem Doulcier, Licence GNU AGPL3+
// ---

///////////////////////////
// INIT GLOBAL VARIABLES //
///////////////////////////
var W = $(window).width(), H = (4/5)*$(window).height() /// Window size
var svg
var getParameterByName, setParameterByName // Defined in uri_variables.js

var t0 = -1
var tf = -1
var file = ''
var color
var skip = 1
var file_list
var coalescence_events

///////////////////////////
// SIMPLE FUNCTIONS     //
///////////////////////////

function getGetOrdinal(n) {
    var s=['th','st','nd','rd']
    var v=n%100
    return n+(s[(v-20)%10]||s[v]||s[0])
}

function timeFormat(num) {
    var h = Math.floor( num / 3600 )
    var m = Math.floor((num - h * 3600) / 60 )
    var s = num - (h * 3600 + m * 60)
    return ( h < 10 ? '0' + h : h ) + ':' + ( m < 10 ? '0' + m : m ) + ':' + ( s < 10 ? '0' + s : s )
}

///////////////////////////
// START WHEN READY      //
///////////////////////////
$(document).ready(function() {
    main()
})

function main() {

    ///////////////////////////
    // SETUP SVG             //
    ///////////////////////////

    var tooltip = d3.select('body').append('div').attr('class', 'tooltip_peaks')
    var margin = 60
    var H_high = H-50
    svg = d3.select('main svg').attr('width', W).attr('height', H).append('g')
    var dotzoom = svg.append('g')
    var peaks_info = svg.append('g')
    var line = svg.append('path')
	.attr('class','line')
	.style('fill','none')
	.style('stroke','steelblue')
	.style('stroke-width','2px')

    ///////////////////////////
    // SETUP UI              //
    ///////////////////////////
    d3.select('div#hud').attr('class','navbar navbar-default').style('margin-top','-20px')
    var hud = d3.select('div#hud').append('form')

    // Select Run
    var runchan = hud.append('fieldset').attr('class','navbar-form navbar-left tipped')
    runchan.append('label').text('Run: ')
    var files_input =  runchan.append('select')
    runchan.append('label').text(' Channel: ')
    var channel_input =  runchan.append('select')

    // Lock and jump on droplet
    var drop_select = hud.append('fieldset').attr('class','navbar-form navbar-left tipped')
    drop_select.append('label').text('Droplet: ')
    var droplet_input = drop_select.append('input').attr('type','number').attr('size',6).style('width','60px').attr('value',getParameterByName('drop'))
    var jump_button = drop_select.append('button').attr('type','button').text('Jump')
    drop_select.append('label').text('Lock')
    var droplet_lock = drop_select.append('input').attr('type','checkbox').property('checked',getParameterByName('drop'))

    // Coalescence event jumper
    var coal_select = hud.append('fieldset').attr('class','navbar-form navbar-center tipped')
    coal_select.append('label').text('Coalescence Event: ')
    var coal_input = coal_select.append('select')
    var coal_before_after = coal_select.append('select')
    coal_before_after.selectAll('option').data(['Before','After']).enter().append('option').text(function(d){return d})
    var jump_coal = coal_select.append('button').attr('type','button').text('Jump')

    // Toggle measure points + skip
    var individual_measure = hud.append('fieldset').attr('class','navbar-form navbar-right tipped')
    individual_measure.append('label').text('Show Measures points')
    var scatter_input =  individual_measure.append('input').attr('type','checkbox')
    individual_measure.append('label').text(' Sampling step: ').style('margin-left','1em')
    var skip_input =  individual_measure.append('input').attr('type','number').attr('value',skip).attr('size',3).style('width','30px').attr('min',1)

    // Help messages
    runchan.attr('data-placement','right').attr('data-toggle','tooltip').attr('title','Load the raw signal for a given run and channel')
    individual_measure.attr('data-placement','left').attr('data-toggle','tooltip').attr('title','Toggle the display of individual measure points. Increasing the sampling step will load less data and might be less ressource heavy')
    coal_select.attr('data-placement','right').attr('data-toggle','tooltip').attr('title','Jump to droplets in the run just before coalescence or the run just after.')
    drop_select.attr('data-placement','right').attr('data-toggle','tooltip').attr('title','Jump to the given droplet. If lock is checked, the signal will stay centered on the droplet even if the channel or run is changed. (You can click on a peak to lock it)')
    $('[data-toggle="tooltip"]').tooltip()

    ///////////////////////////
    // SETUP SCALES & AXES   //
    ///////////////////////////

    // Main plot
    var x = d3.scaleLinear().range([margin, W-margin])
    var y = d3.scaleLinear().range([H_high-margin, margin])

    // `x_all` and `y_all` are for the brush below.
    var x_all = d3.scaleLinear().range([margin, W-margin]).clamp(true)
    var y_all = d3.scalePoint().range([H-22, H_high])


    // Axes
    var x_allaxis = svg.append('g')
	.attr('transform', 'translate(0,' + (H-20) + ')')
    var xaxis = svg.append('g')
	.attr('transform', 'translate(0,' + (H_high-60) + ')')
    var yaxis = svg.append('g')
	.attr('transform', 'translate(50, 0)')

    ///////////////////////////
    // SETUP BRUSH           //
    ///////////////////////////

    var brush = d3.brushX()
	.extent([[0, H_high], [W, H]])
	.on('end', brushed)
    var gbrush = svg.append('g')
	.attr('class', 'brush')
	.call(brush)

    function brushed(){
	if (!d3.event.sourceEvent) return // If called programatically sourceEvent is null. Usefull to move the brush without updating.
	var s = d3.event.selection || [x_all(t0),x_all(tf)]
	s = s.map(x_all.invert, x_all)
	if (Math.round(s[0]) != t0 || tf != Math.round(s[1])){
	    t0 =  Math.max(Math.round(s[0]),x_all.domain()[0])
	    tf =  Math.min(Math.round(s[1]),x_all.domain()[1])
	    update(false)
	}
    }

    ///////////////////////////
    // KEYBINDINGS           //
    ///////////////////////////

    $(document).keydown(function(e) {
	var dt = Math.abs(tf - t0)/3
	switch(e.which) {
	case 37: // left
	    dt *= -1
	    break
	case 39: // right
	    break
	default: return // exit this handler for other keys
	}
	t0 =  Math.max(t0+dt,x_all.domain()[0])
	tf =  Math.min(tf+dt,x_all.domain()[1])
	update()
	e.preventDefault() // prevent the default action (scroll / move caret)
    })

    //////////////////////////////////////////////
    // BIND UI ACTIONS                          //
    //////////////////////////////////////////////

    jump_coal.on('click', action_jump_coal)
    jump_button.on('click', action_jump)
    channel_input.on('change', update)
    scatter_input.on('change', update)
    files_input.on('change', function(){
	file = files_input.node().options[files_input.node().selectedIndex].value
	load_new_file()})
    skip_input.on('change', update)

    //////////////////////////////////////////////
    // LOAD THE LIST OF RAW DATA FILES          //
    //////////////////////////////////////////////

    var err = d3.json('metadata?run=list',
		      function(error, json){
			  if (error){
			      hud.attr('class','alert alert-danger')
			      hud.html('<strong> Error: </strong> You need to start dropsignal server to be able to use the raw data explorer. <a href="runs.html">Go to the legacy signal explorer.</a><br/> If you are reading data from an old version of dropsignal, please run the analysis again.')
			      return 1
			  }

			  // Init some global variables
			  file_list = json.runs
			  file = getParameterByName('run')?getParameterByName('run'):file_list[0]
			  color = json.color_group
			  coalescence_events = json.coalescence_events

			  // Set the UI

			  // Optionally remove the suffix '_max,_mean'... so links from the scatter works.
			  var chan = getParameterByName('channel')
			  if (chan != null && chan.split('_').length>2) {
			      chan = chan.split('_').slice(0, 2).join('_')
			  }

			  channel_input.selectAll('option').data(json.channels).enter().append('option')
			      .attr('value',function(d){return d}).text(function(d){return d})
			      .property('selected', function(d){return d==(chan?chan:'detection_1')})
			  files_input.selectAll('option').data(file_list).enter().append('option')
			      .attr('value',function(d){return d}).text(function(d){return d})
			      .property('selected', function(d){return d==file})
			  coal_input.selectAll('option').data(json.coalescence_events).enter().append('option')
			      .attr('value',function(d,i){return i}).text(function(d){return 'Run '+d[0]+' Peak '+d[1]})

			  // Load the first dataset.
			  load_new_file()
			  return 0
		      })

    if (err == 1){
	return
    }
    //////////////////////////////////////////////
    // FUNCTIONS                                //
    //////////////////////////////////////////////

    function load_new_file(){
	console.log('metadata?file='+file)
	d3.json('metadata?run='+file,
		function(json){
		    console.log('Opened file:'+file, json)

		    // Update axes
		    x_all.domain([json.t0,json.tf])
		    x_allaxis.call(d3.axisBottom(x_all).tickFormat(timeFormat))
		    y_all.domain(json.peaks.map(function(d){return d.group}))

		    // If the global t0 and tf are possible keep them otherwise update them
		    function convert(d){return d===null?-1:+d}
		    if (json.t0 <= convert(getParameterByName('t0')) && json.tf >= convert(getParameterByName('tf'))){
			t0 = +getParameterByName('t0')
			tf = +getParameterByName('tf')
		    }
		    else{
			t0 = json.t0
			tf = json.t0 + (json.tf-json.t0)/50
		    }

		    // Put a small circle per peak in the bursh zone.
		    var cir = dotzoom.selectAll('circle').data(json.peaks)
		    console.log('Peaks in run '+file+' : '+json.peaks.length)
		    cir.enter()
			.append('circle')
			.attr('r',1)
			.merge(cir)
			.attr('cx', function(d) { return x_all(d.time) })
			.attr('cy', function(d) { return y_all(d.group)})
			.style('fill',function(d){return color[d.group]})
			.style('stroke','none')
		    cir.exit().remove()

		    // Draw
		    droplet_lock.property('checked') ? action_jump() : update()
		})
    }

    function update(update_brush){
	///// General update ////

	// If we are locked and we are not brushing
	// to remove the lock, we jump.
	// Otherwise we unlock and update as usual.
	if (droplet_lock.property('checked')){
	    if ( update_brush === undefined){
		action_jump()
		return
	    }
	    else{
		droplet_lock.property('checked', false)
	    }
	}

	// Get variables form UI
	var chan = channel_input.node().options[channel_input.node().selectedIndex].value
	var skip = skip_input.property('value')
	var scatter = scatter_input.property('checked')

	// Update URL
	var new_URL = setParameterByName(window.location.href,'run',file)
	new_URL = setParameterByName(new_URL,'channel',chan)
	new_URL = setParameterByName(new_URL,'t0',t0)
	new_URL = setParameterByName(new_URL,'tf',tf)
	new_URL = setParameterByName(new_URL,'drop')
	window.history.replaceState( {} , '', new_URL)

	// Get data and display.
	var request = 'signal?t0='+t0+'&tf='+tf+'&skip='+skip+'&channel='+chan+'&run='+file
	console.log('UPDATE', request)
	d3.json(request, function(json){
	    display(json, scatter)
	})
    }

    function action_jump(){
	///// Jump to droplet ////

	// Get variables form UI
	var chan = channel_input.node().options[channel_input.node().selectedIndex].value
	var skip = skip_input.property('value')
	var scatter = scatter_input.property('checked')
	var drop = droplet_input.property('value')

	// Update URL
	var new_URL = setParameterByName(window.location.href,'run',file)
	new_URL = setParameterByName(new_URL,'channel', chan)
	new_URL = setParameterByName(new_URL,'t0')
	new_URL = setParameterByName(new_URL,'tf')
	new_URL = setParameterByName(new_URL,'drop', drop)
	window.history.replaceState( {} , '', new_URL )

	// Get data and display
	var request = 'signal?drop='+drop+'&skip='+skip+'&channel='+chan+'&run='+file
	console.log('ACTION: JUMP TO '+drop, request)
	d3.json(request, function(json){
	    display(json, scatter)
	    t0 = json.data[0][0]
	    tf = json.data[json.data.length-1][0]
	})
    }

    function action_jump_coal(){
	///// Jump to coalescence event ////

	// Get variables form UI
	var event = coal_input.node().options[coal_input.node().selectedIndex].value
	var old_file = file
	file = coalescence_events[event][0]-1+coal_before_after.property('selectedIndex')

	// Update UI so the droplet we jump to is locked and the run is right.
	files_input.selectAll('option').each(function(){
	    if(this.value == file){files_input.property('selectedIndex',this.index)}})
	droplet_input.property('value',coalescence_events[event][1])
	droplet_lock.property('checked',true)

	// Get the data and jump...
	if (old_file != file) { load_new_file() }
	action_jump()
    }


    function display(json, scatter){
	///// Display the data ////
	console.log('DISPLAY '+json.peaks.length+' peaks in this region', json)

	// Axes
	var xextent = d3.extent(json.data.map(function(d){return d[0]}))
	x.domain(xextent)
	y.domain([0,d3.max(json.data.map(function(d){return d[1]}))])
	xaxis.call(d3.axisBottom(x).tickFormat(timeFormat))
	var yaxis_obj = d3.axisRight(y)
	    .tickSize(Math.abs(x.range()[0]-x.range()[1]))
	yaxis.call(yaxis_obj)
	yaxis.select(".domain").remove();
	yaxis.selectAll(".tick line")
	    .attr("stroke", "#777")
	    .attr("stroke-width", "0.3")
	yaxis.selectAll(".tick text").attr("x", 4).attr("dy", -4);

	// Move the brush
	gbrush.call(brush.move, [x_all(xextent[0]),x_all(xextent[1])])

	// Trace the actual signal
	var valueline = d3.line()
	    .x(function(d) {return x(d[0])})
	    .y(function(d) {return y(d[1])})
	line.data([json.data]).attr('d', valueline)

	// Scatter points
	var small_dots = svg.selectAll('.dot')
	    .data(scatter ? json.data : [])
	small_dots.enter().append('circle')
	    .attr('class','dot')
	    .attr('r', 2)
	    .merge(small_dots)
	    .attr('cx', function(d) { return x(d[0]) })
	    .attr('cy', function(d) { return y(d[1]) })
	small_dots.exit().remove()

	// Peaks group.
	// Composed of a rectangle, and the text
	// Tooltip on mouseover, and jump on click.
	var peak = peaks_info.selectAll('g').data(json.peaks)
	var peak_enter = peak.enter().append('g')
	peak_enter.append('rect').attr('class','position_rect')
	peak_enter.append('rect').attr('class','value_rect')
	peak_enter.append('text').attr('class','label')
	peak_enter.append('line')
	peak_update = peak_enter.merge(peak)
	    .attr('transform', function(d){return 'translate('+x(d.time)+','+y.range()[1]+')'})
	    .on('mouseover', update_tooltip)
	    .on('click',function(d){
		droplet_input.property('value', d.id_exp)
		droplet_lock.property('checked', true)
		action_jump()
	    })
	    .on('mouseout', function() {
		tooltip.style('display', 'none')})
	    .on('mousemove', function() {
		tooltip.style('top', (d3.event.layerY + 10) + 'px')
		       .style('left', (d3.event.layerX + 10) + 'px')
	    })
	peak_update.select('line')
	    .attr("stroke", "#777")
	    .attr("stroke-width", "0.1")
	    .attr("y1",y(0)-y.range()[1])
	    .attr("y2",0)
	// A transparant rectangle that show the position of the
	// measurment window and span the full height of the y-scale
	peak_update.select('.position_rect')
	    .attr('height', Math.abs(y.range()[0]-y.range()[1]))
	    .attr('y', 0)
	    .attr('opacity',.2)
	    .attr('x',function(d){return x(d.start+(d.shift?d.shift:0))-x(d.time)})
	    .attr('width',function(d){return x(d.end)-x(d.start)})
	    .style('fill',function(d){return color[d.group]})
	// A less transparant rectangle that show the position of the
	// measurment window and stops at the median of the peak.
	peak_update.select('.value_rect')
	    .attr('height',function (d) { return Math.abs(y(d.median)-y.range()[0])})
	    .attr('y',function(d){return y(d.median)-y.range()[1]})
	    .attr('opacity',.8)
	    .attr('x',function(d){return x(d.start+(d.shift?d.shift:0))-x(d.time)})
	    .attr('width',function(d){return x(d.end)-x(d.start)})
	    .style('fill',function(d){return color[d.group]})
	peak_update.select('text')
	    .style('text-anchor', 'middle')
	    .style('font-family', 'monospace')
	    .style('fill', function(d){return d.id_exp != -1? color[d.group] :'#bab0ac'})
	    .attr('dy', '-5')
	    .text(function(d) {return d.id_exp != -1 ? d.id_exp : '('+d.id_run+')'})
	peak.exit().remove()
    }

    function update_tooltip(d){
	/// Udpate the tooltip when mouseover///
	var span_open = ' <span class="label" style="margin-right:.1em;background-color:'+color[d.group]+'"> '
	var span_close = ' </span> '

	if (d.id_exp != -1){
	    var txt = span_open + 'Droplet: ' + d.id_exp + span_close
		+ span_open + 'Well: ' + d.well + span_close
		+ span_open + d.group + span_close
	} else{
	    txt= '<span class="label label-danger">Droplet assignment failed</span>'
	}

	txt+= '<br/><span class="label label-default">'+getGetOrdinal(d.id_run)+' peak in this run</span>'
	txt+= '<table  style="width:100%" class="table-condensed table-bordered"><tr><th>Time</th><td>'+timeFormat(Math.round(d.time))+'</td></tr>'

	var fmt = d3.format('.5')
	var fmt_cov = d3.format('%')

	if (d.value){
	    txt+= '<th>Value</th><td> '+fmt(d.value)+'</td></tr>'
	    txt+= '<th>Median</th><td> '+fmt(d.median)+'</td></tr>'
	    txt+= '<th>Std</th><td> '+fmt(d.std)+'</td></tr>'
	    txt+= '<th>C. of Var.</th><td> '+fmt_cov(d.cov)+'</td></tr>'
	    txt+= '<th>Max</th><td>'+fmt(d.max)+'</td></tr>'
	    txt+= '<th>Area</th><td>'+fmt(d.area)+'</td></tr>'}

	txt += '</table>'

	fmt = d3.format('.3s')
	if (d.shift){
	    txt+= '<span class="label label-default">'+fmt(d.shift)+'s offset from photodiode</span>'
	}

	tooltip.html(txt)
	tooltip.style('display', 'block')
	tooltip.style('text-align', 'center')
    }
}
