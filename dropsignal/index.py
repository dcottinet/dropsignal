"""index.py create an html file listing the analysed experiments."""
from glob import glob
import os
from itertools import chain
from collections import defaultdict
import logging
import json
import time
import dropsignal.export
from dropsignal import __version__

logger = logging.getLogger('dropsignal')

def discover_and_index(path):
    """Crawl the path looking for experiment, and build an index.
    """
    logger.info("===== Indexing =====")

    analysis = []
    newstatic = dropsignal.export.static(path, back_link="/")
    newstatic = os.path.relpath(newstatic, start=path)

    for folder in chain(glob(os.path.join(path, '*', 'analysis')),
                        glob(os.path.join(path, '*', '*', 'analysis'))):
        if os.path.exists(folder):
            logger.debug("Found {}".format(folder))
            protocol = defaultdict(lambda: "unknown", path=folder)
            try:
                with open(os.path.join(folder, 'metadata.json')) as fi:
                    protocol.update(json.loads(fi.read()))
            except OSError as ex:
                protocol["status"] = "Error"
                logger.warning("{}: {}".format(folder, ex))
            analysis.append(protocol)

    # Building the HTML Index
    write_index(analysis, path, newstatic)


def write_index(analysis, path, static_path):
    """HTML experiments index"""
    output = '''<html charset="UTF-8">
    <title>Experiments index</title>
    <link href="{0}/lib/css/bootstrap.min.css" rel="stylesheet">
    <link href="{0}/lib/css/dc.css" type="text/css" rel="stylesheet">
    <link href="{0}/css/main.css" rel="stylesheet" type="text/css">
    <body>
    <div id="container">
    <header class="navbar navbar-default">
    <div class="navbar-header">
    <a class="navbar-brand" href="#">Droplet machine experimental database </a>
    <ul class="nav navbar-nav">
    <li class="active"><a href="#">Index</a></li>
    </ul>
    </div>
    </div>
    <div class='container'>
    <h1> Experiments index </h1>'''.format(static_path)

    def sorter(row):
        """ Sort the table row by time then path """
        try:
            date = time.strptime(row["time_first"], "%Y-%m-%d %H:%M:%S")
        except ValueError:
            date = time.gmtime(0)
        return (date, row['path'])

    output += "<table class='table'>"
    output += "<tr><th>Path</th><th>Time</th><th>Duration</th><th>Runs</th><th>Processing</th></tr>"
    for line in sorted(analysis, key=sorter, reverse=True):
        if "title" not in line:
            line["title"] = line["path"].replace("/analysis", "")
        if 'pipeline_infos' not in line:
            line['pipeline_infos'] = {"timestart":"unknown"}
        if "version" not in line['pipeline_infos']:
            line['pipeline_infos']["version"] = "unknown"
        line["path"] = os.path.relpath(line["path"], start=path)
        status_class = {"OK":"success", 'unknown':'info', "Running":"warning", "Error":"danger"}[line["status"]]
        if line["pipeline_infos"]["version"] == __version__:
            line["pipeline_infos"]["version"] = '<strong title="current">'+line["pipeline_infos"]["version"]+"</strong>"
        output += ("<tr>"
                   "<td><a href={}>{}</a></td>"
                   "<td >{}</td>"
                   "<td>{}</td>"
                   "<td>{}/{}</td>"
                   "<td class='{}'>{} ({})</td>"
                   "</tr>").format(line["path"],
                                   line["title"],
                                   line["time_first"],
                                   line["duration"].replace("0 days", ""),
                                   line["good_runs"],
                                   line["runs"],
                                   status_class,
                                   line["pipeline_infos"]["version"],
                                   line["pipeline_infos"]["timestart"])
    output += "</table></div>"
    output += dropsignal.export.footer()

    idx_path = os.path.join(path, 'index.html')
    try:
        with open(idx_path, "w") as file:
            file.write(output)
    except PermissionError as error:
        logger.exception(error)

    logger.info("Wrote {}".format(idx_path))
