import re
LOG_FORMAT = {'DEBUG':'<span class="label label-default">Debug</span>',
              'INFO':'<span class="label label-primary">Info</span>',
              'CRITICAL':'<span class="label label-danger">Critical</span>',
              'ERROR':'<span class="label label-danger">Error</span>',
              'WARNING':'<span class="label label-warning">Warning</span>'}
LOG_REGEXP = re.compile('|'.join(map(re.escape, LOG_FORMAT.keys())))
def format_log(line):
    if '---' in line:
        return '<h4>'+line.split('---')[1]+'</h4>'
    if '~~~' in line:
        return '<h2>'+line.split('~~~')[1]+'</h2>'
    return LOG_REGEXP.sub(lambda match: LOG_FORMAT[match.group(0)], line)
