# Droplet signal processing

Extraction, conversion and visualization of experimental data from
millifluidic droplet trains.

## Installation

If you have Python 3 along with the `pip`, installing the current
stable version of `dropsignal` can be done by:

```
pip install dropsginal
```

installing the current development version of `dropsignal` can be done
by:

```
pip install https://gitlab.com/evomachine/dropsignal/repository/archive.zip?ref=master
```

For more information see
the
[User's manual](https://evomachine.gitlab.io/dropsignal/manual.html#installation-and-update).

## Quick start

Once dropsignal is installed, use the command `dropsignal_web --browser PATH`
to list all the experiments in `PATH`. From there you can edit the
protocol and start the analysis with the corresponding buttons. For
more information see the [User's manual](https://evomachine.gitlab.io/dropsignal/manual.html).

## Documentation

The documentation for the current version of dropsignal can be found here:

https://evomachine.gitlab.io/dropsignal/


### Building the documentation
If `dropsignal` and [sphinx](http://www.sphinx-doc.org) are installed
you can build the documentation for your current version by running
`make html` in the documentation folder of this repository.

## Contributing

This is free and open-source software, you are encouraged to read,
study, modify and redistribute the source code.

The official place to contribute is the
[gitlab repository `evomachine/dropsignal`](https://gitlab.com/evomachine/dropsignal). Feel
free to:

- report issues or propose enhancement in our [issue tracking system](https://gitlab.com/evomachine/dropsignal/issues).
- propose patches and pull requests.


### Complete Dependencies

The pipeline is written in Python 3 and uses the following packages for data processing:

- **[numpy](http://www.numpy.org/)**: general scientific computing library.
- **[scipy](http://scipy.org/)**: signal smoothing (convolution) and model fitting.
- **[pandas](http://pandas.pydata.org/)**: dataframe implementation.
- **[matplotlib](http://matplotlib.org/)**: static figures generation.
- **[cherrypy](http://cherrypy.org/)**: webserver and application framework.

Data visualization is made in the browser using the following
libraries (included in the `dropsignal/static/lib`
directory):

- **[jquery](https://jquery.com/)** Javascript framework.
- **[d3.js](https://d3js.org/)**: Linking data to the DOM.
- **[bootstrap](https://getbootstrap.com/)**: Clean looking CSS.
- **[anime.js](http://animejs.com)**: Loader animation.

## License

Copyright (c) 2016-2018 Guilhem Doulcier

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
