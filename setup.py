try:
    from setuptools import setup, find_packages
except ImportError:
    from distutils.core import setup
    try:
        from dropsignal.plugins import INSTALLED
        find_packages = lambda: ['dropsignal','dropsignal.plugins'] + ['dropsignal.plugins.{}'.fromat(i) for i in INSTALLED]
    except Exception as ex:
        find_packages = lambda: ['dropsignal','dropsignal.plugins']

try:
    from dropsignal import __version__
except Exception as ex:
    __version__ = "unknown"

setup(name='dropsignal',
      version=__version__,
      description='Millifluidic droplet trains analysis',
      url='https://gitlab.com/groups/evomachine',
      author='Guilhem Doulcier',
      long_description='''Dropsignal is a a free and open source software performing the
extraction, conversion and visualization of experimental data obtained from
millifluidic droplet trains..''',
      author_email='guilhem.doulcier@espci.fr',
      include_package_data=True,
      license='AGPLv3',
      python_requires='>=3',
      classifiers=[
          'Development Status :: 4 - Beta',
          'Intended Audience :: Science/Research',
          'License :: OSI Approved :: GNU Affero General Public License v3',
          'Programming Language :: Python :: 3 :: Only',
          'Topic :: Scientific/Engineering :: Bio-Informatics',
          'Topic :: Scientific/Engineering :: Visualization'
      ],
      packages=find_packages(),
      install_requires=[
          'numpy',
          'scipy',
          'pillow',
          'pandas>=0.16.2',
          'matplotlib',
          'xlwt',
          'openpyxl',
          'mypy-lang',
          'cherrypy'
      ],
      entry_points={
          'console_scripts': [
              'dropsignal=dropsignal.pipeline:main',
              'dropsignal_web=dropsignal.webgui:main',
              'dropsignal_gui=dropsignal.gui:main'
          ]
      }
)
